/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.util.SimpleByteSource;
/**
 *
 * @author Marcelino
 */
public class CustomHashedCredentialsMatcher extends HashedCredentialsMatcher
{
  @Override
  protected Object hashProvidedCredentials(AuthenticationToken token, AuthenticationInfo info)
  {
    if (info instanceof CustomAuthenticationInfo)
    {
      CustomAuthenticationInfo cai = (CustomAuthenticationInfo)info;      
      setHashIterations(cai.getHashIterations());
    }

    return super.hashProvidedCredentials(token, info);
  } 
}