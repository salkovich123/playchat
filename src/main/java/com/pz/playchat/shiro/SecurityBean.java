/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.shiro;


import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.User;
import com.pz.playchat.utils.JsfUtils;
import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.omnifaces.util.Faces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
/**
 *
 * @author Marcelino
 */
@Named("securityBean")
@SessionScoped
public class SecurityBean implements Serializable
{
    private String timezone;
  /**
   * Logger for the class
   */
  private final static Logger logger = LoggerFactory.getLogger(SecurityBean.class);
  
  /**
   * Marker that severity of error is fatal
   */
  private final static Marker fatal = MarkerFactory.getMarker("FATAL");
  
  /**
   * URL of the default (welcome) page
   */
  
  private static final String URL_HOME_ADMIN = "admin/index.xhtml";
  private static final String URL_USERS = "users/index.xhtml";
  
  /**
   * URL of the default (login) page
   */
  private static final String URL_LOGIN =  "login.xhtml";
  private static final String URL_INDEX =   "index.xhtml";
  
  /**
   * ID of p:messages component used for displaying error messages
   */
  private final static String P_MESSAGES_ID = "loginMessages";
  
  /**
   * Username entered by user
   */
  private String username;
  
  /**
   * Password entered by user
   */
  private String password;
  
  
  @EJB
  UserJpaController userController;
  
  /*
  + Remember me boolean for remember me functionality
  */
  private boolean rememberMe;
  
  @PostConstruct
  public void init()
  {
      
    System.out.println("Login bean initialized!");
   
  }

  /**
   * @return the username
   */
  public String getUsername()
  {
    return username;
  }

  /**
   * @param username the username to set
   */
  public void setUsername(String username)
  {
    this.username = username;
  }

  /**
   * @return the password
   */
  public String getPassword()
  {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  /**
   * Returns Apache Shiro's Subject associated with this Session
   * @return Subject of Apache Shiro Security library
   */
  public Subject getSubject()
  {
    return SecurityUtils.getSubject();
  }
  
  /**
   * Sets username and password to null
   */
  private void resetUsernameAndPassword()
  {
    username = null;
    password = null;
    rememberMe = false;
  }
  
   
  
  /**
   * Tries to perform login to the application with supplied username and password
   */
  public void login()
  { 
    try
    {
        System.out.println("PIZDA:::::"   + timezone);
      // Check if is username / password is entered
      if ( (username == null) || (password == null) )
      {
        throw new IllegalArgumentException();
      }
      
      // Try to login and if it succeeds reset username / password
      SecurityUtils.getSubject().login(new UsernamePasswordToken(username, password, rememberMe));
     long id = (long)SecurityUtils.getSubject().getPrincipal();
      User user  = userController.findUser((int)id);
      user.setUserloginTime(new Date());
      userController.edit(user);
      resetUsernameAndPassword();
      
      SavedRequest savedRequest = WebUtils.getAndClearSavedRequest((ServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest());
      System.out.println("ROLE FOR USER: " + SecurityUtils.getSubject().hasRole("God"));
      
      if (savedRequest == null)
      {
        if(SecurityUtils.getSubject().hasRole("Admin"))
            FacesContext.getCurrentInstance().getExternalContext().redirect(URL_HOME_ADMIN);
          else
              FacesContext.getCurrentInstance().getExternalContext().redirect(URL_USERS);
      }
      
      else
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect(savedRequest.getRequestUrl());
      }
    }
    
    catch(IllegalArgumentException e)
    {
      resetUsernameAndPassword();
      JsfUtils.addMessage(JsfUtils.SEVERITY_ERROR, "Log in failed", "Username and (or) password must be entered!", P_MESSAGES_ID);
    }
    
    catch(AuthenticationException e)
    {
      resetUsernameAndPassword();
      JsfUtils.addMessage(JsfUtils.SEVERITY_ERROR, "Log in failed!", "Invalid username and (or) password!", P_MESSAGES_ID);
      e.printStackTrace();
    }
    
    catch(IOException e)
    {
      resetUsernameAndPassword();
      logger.error(fatal, "User failed to login due system error!", e);
      JsfUtils.addMessage(JsfUtils.SEVERITY_FATAL, "Log in failed!", "Fatal error: " + e.getMessage());
    } catch (NonexistentEntityException ex) {
          java.util.logging.Logger.getLogger(SecurityBean.class.getName()).log(Level.SEVERE, null, ex);
      } catch (RollbackFailureException ex) {
          java.util.logging.Logger.getLogger(SecurityBean.class.getName()).log(Level.SEVERE, null, ex);
      } catch (Exception ex) {
          java.util.logging.Logger.getLogger(SecurityBean.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
  
 /**
   * Logs out current user from application by performing following operations:
   * logs user from ApacheShiro library, does FacesLogout, invalidates HTTP session
   * and tries to redirect to login page
   * @throws IOException when redirect to Login page fails for some reason
   */
  public void logout() throws IOException
  {
    doShiroLogout(); // Perform logout from ApacheShiro security
    
    doFacesInvalidateSession(); // Invalidates HTTP session
    
    Faces.redirect(URL_INDEX); // Redirect to login page
  }
  
  /**
   * Performs logout from ApacheShiro library
   */
  private void doShiroLogout()
  {
    try
    {
      Subject subject = SecurityUtils.getSubject();
      
      if (subject != null)
      {
        subject.logout();
      }
    }
    
    catch(Exception e) // TO DO: ensure that this makes sense!
    {
      logger.error(fatal, "Failed to log off user from ApacheShiro library", e);
    }
  }
  
  /**
   * Invalidates HTTP session
   * @see org.omnifaces.util.Faces
   */
  private void doFacesInvalidateSession()
  {
    try
    {
      Faces.invalidateSession();
    }
    
    catch(Exception e)
    {
      logger.error(fatal, "Failed to logout user by invalidating session", e);
    }   
  }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
  
}