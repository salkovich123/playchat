/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.shiro;

import com.pz.playchat.utils.SqlUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.SimpleByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
/**
 *
 * @author Marcelino
 */
public class CustomJdbcRealm extends AuthorizingRealm
{ 
  /**
   * Query for extracting stored user's id, username and password from database
   */
  private final static String QUERY_CREDENTIALS_PRINCIPALS = "SELECT User_password, User_passwordSalt, User_saltIteration, User_confirmed, \n" +
                                                            " idUser \n" +
                                                             "FROM User \n" +
                                                             "WHERE BINARY User_username = ?";
  
  /**
   * Query for extracting user's roles and its permissions from the database
   */
  private final static String QUERY_ROLES = "SELECT r.Role_name FROM  Role r where r.idRole = (Select u.Role_idRole FROM User u WHERE u.idUser =?)";
  
  /**
   * Marker that severity of error is fatal
   */
  private final static Marker fatal = MarkerFactory.getMarker("FATAL");

  /**
   * SLF4J logger for this class
   */
  private static final Logger logger = LoggerFactory.getLogger(CustomJdbcRealm.class);
  
  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken at) throws AuthenticationException
  {
    Connection connection = null;
    int confirmed=0;
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;
    
    UsernamePasswordToken upt = (UsernamePasswordToken)at;
    Long userId = null;
    char[] passwordHashArray = null;
    SimpleByteSource passwordSalt = null;
    int passwordIterations = 0;
    
    try
    {
      connection = getConnection();
      System.out.println("QUERY     " + QUERY_CREDENTIALS_PRINCIPALS);
      preparedStatement = connection.prepareStatement(QUERY_CREDENTIALS_PRINCIPALS);
      preparedStatement.setString(1, upt.getUsername());

      resultSet = preparedStatement.executeQuery();
      boolean resultFound = false;
      
      while(resultSet.next())
      {
        // Read user_id, user_username and user_password from database
        userId = new Long(resultSet.getLong("idUser"));
        String passwordHash = resultSet.getString("User_password");
        passwordSalt = new SimpleByteSource(resultSet.getBytes("User_passwordSalt"));
        passwordIterations = resultSet.getInt("User_saltIteration");
        confirmed = resultSet.getInt("User_confirmed");
        
        System.out.println("User password: " + passwordHash);
        
        if (passwordHash != null)
        {
          passwordHashArray = passwordHash.toCharArray();
        }
        
        if (!resultFound)
        {
          resultFound = true;
        }
        
        else
        {
          String message = "Multiple user accounts found for user: " + upt.getUsername();
          logger.error(fatal, message);
          throw new AccountException(message);
        }
      }
      
      if (!resultFound)
      {
        throw new UnknownAccountException("Cannot find user's account for username: " + upt.getUsername());
      }
    }
    
    catch(SQLException e)
    {
      String message = "Failed to authenticate user due SQLException";
      logger.error(fatal, message, e);
      throw new AuthenticationException(message, e);
    }
    
    catch(AccountException e)
    {
      throw e;
    }
    
    finally
    {
      SqlUtils.closeQuietly(connection, preparedStatement, resultSet);
    }
    CustomAuthenticationInfo sai = null;
   if(confirmed >= 0){
    // Construct username & password credentials
    sai = new CustomAuthenticationInfo(userId, passwordHashArray, passwordSalt, getName(), passwordIterations);
   }else{
        try {
            throw new CredentialsException("You must confirm your account before login");
        } catch (CredentialsException ex) {
            java.util.logging.Logger.getLogger(CustomJdbcRealm.class.getName()).log(Level.SEVERE, null, ex);
        }
        sai = null;
   }
    return sai;
  }
  
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection pc)
  {
    Iterator i = pc.iterator();
    long id = 0;
    while(i.hasNext())
    {
        id=  (long) i.next();
      System.out.println("Authorization principal: " + id);
    }
    
    Connection connection = null;
    Set<String> roles = new HashSet<>();
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;
    
    try
    {
      connection = getConnection();
      preparedStatement = connection.prepareStatement(QUERY_ROLES);
      preparedStatement.setLong(1,(int) id); // TO DO add userId
      
      resultSet = preparedStatement.executeQuery();
      
      while(resultSet.next())
      {
        roles.add(resultSet.getString("Role_name")); 
      }
    }
    
    catch(SQLException e)
    {
      String message = "Failed to authorize user due SQLException";
      logger.error(fatal, message, e);
      throw new AuthorizationException(message, e);
    }
    
    finally
    {
      SqlUtils.closeQuietly(connection, preparedStatement, resultSet);
    }
    
    // Build SimpleAuthorizationInfo to return roles  for user
    SimpleAuthorizationInfo sai = new SimpleAuthorizationInfo();
    sai.setRoles(roles);
    
    return sai;
  }
  
  private Connection getConnection()
  {
    Connection c = null;
    
    try
    {
      InitialContext ic = new InitialContext();
      DataSource dataSource = (DataSource)ic.lookup("jdbc/PlayPool");
    
      c = dataSource.getConnection();
    }
    
    catch(Exception e)
    {
      System.out.println("FAILED!");
    }
    
    return c;
  }
}