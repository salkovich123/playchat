/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.shiro;

import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.util.ByteSource;
/**
 *
 * @author Marcelino
 */
public class CustomAuthenticationInfo extends SimpleAuthenticationInfo
{
  /**
   * Number of iterations to calculate correct user's hash
   */
  private int hashIterations;
  
    /**
     * Creates new instance of CustomAuthenticationInfo object. All parameters of this constructor with exception
     * of hashIterations are forwarded to constructor of parent class.
     * @param principal the primary principal associated with the specified realm.
     * @param hashedCredentials the hashed credentials that verify the given principal.
     * @param credentialsSalt the salt used when hashing the given hashedCredentials
     * @param realmName the realm from where the principal and credentials were acquired.
     * @see org.apache.shiro.authc.credential.HashedCredentialsMatcher HashedCredentialsMatcher
     * @see org.apache.shiro.authc.SimpleAuthenticationInfo
     */
  public CustomAuthenticationInfo(Object principal, Object hashedCredentials, ByteSource credentialsSalt, String realmName, int hashIterations)
  {
    // Call constructor of super class
    super(principal, hashedCredentials, credentialsSalt, realmName);
    
    // Store hash iterations
    this.hashIterations = hashIterations;
  }

  /**
   * Returns number of iterations needed to calculate correct password hash
   * @return number of iterations needed to calculate correct password hash
   */
  public int getHashIterations()
  {
    return hashIterations;
  }

  /**
   * Sets number of iterations needed to calculate correct password hash
   * @param hashIterations number of iterations needed to calculate correct password hash
   */
  public void setHashIterations(int hashIterations)
  {
    this.hashIterations = hashIterations;
  }
}