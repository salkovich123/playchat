/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.entity.ChatRoomUsers;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Marcelino
 */
@Named(value = "transferDataBean")
@SessionScoped
public class TransferDataBean implements Serializable {
    
    private ChatRoomUsers roomUsers;

    public ChatRoomUsers getRoomUsers() {
        return roomUsers;
    }

    public void setRoomUsers(ChatRoomUsers roomUsers) {
        this.roomUsers = roomUsers;
    }
    
    
    
}
