/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.ChatRoomUsersJpaController;
import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.User;
import com.pz.playchat.shiro.Configuration;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author Marcelino
 */
@Named(value = "lobbyBean")
@ViewScoped
public class LobbyBean implements Serializable {
    String folder;
    @EJB
    ChatRoomUsersJpaController roomPrivilegeJpa;
    @EJB
    UserJpaController userJpa;
    
    private List<User> usersInGroup;
    private List<ChatRoomUsers> privilegeRooms;
            
    @PostConstruct
    public void init(){
        folder = Configuration.folderPath;
        System.out.println("INIT lobbyBean  method");
        long id = (long) SecurityUtils.getSubject().getPrincipal();
        this.privilegeRooms = roomPrivilegeJpa.findRoomsByUser(userJpa.findUser((int) id));
        System.out.println("privilege rooms size " + privilegeRooms.size() );
        
    }
    public int getNumUsersLobby(ChatRoomUsers room){
        System.out.println("getNumUsersLobby");
       return roomPrivilegeJpa.countUsersInRoom(room.getChatRoomidChatRoom());
    }
    
    public void setUsersDialog(ChatRoomUsers room){
        System.out.println("setUsersDialog");
        usersInGroup = roomPrivilegeJpa.ChatRoomUsers(room.getChatRoomidChatRoom());
     
    }
    
    
    public List<ChatRoomUsers> getPrivilegeRooms() {
        return privilegeRooms;
    }

    public void setPrivilegeRooms(List<ChatRoomUsers> privilegeRooms) {
        this.privilegeRooms = privilegeRooms;
    }

    public List<User> getUsersInGroup() {
        return usersInGroup;
    }

    public void setUsersInGroup(List<User> usersInGroup) {
        this.usersInGroup = usersInGroup;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
    
    
}
