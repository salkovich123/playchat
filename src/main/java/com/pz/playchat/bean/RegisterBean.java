/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.ChatRoomUsersJpaController;
import com.pz.playchat.chat.controller.IOController;
import com.pz.playchat.chat.controller.RoleJpaController;
import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.Role;
import com.pz.playchat.chat.entity.User;
import com.pz.playchat.mail.ConfirmationMail;
import com.pz.playchat.mail.Mail;
import com.pz.playchat.shiro.Configuration;
import java.io.File;
import java.io.IOException; 
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.util.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author Marcelino
 */
@Named(value = "registerBean")
@ViewScoped
public class RegisterBean implements Serializable {
    
    User  user;
    @EJB
    RoleJpaController roleC;
    @EJB
    UserJpaController userC;
    @EJB
    ChatRoomJpaController roomJpa;
    @EJB
    ChatRoomUsersJpaController roomUsersJpa;
            
    @EJB
    IOController ioc;
    final String confirmationUrl = Configuration.serverPath+ "confirmMail.xhtml?key=";
    
    @PostConstruct
    public void init(){
    
        
        user = new User();
    }
    public void register(){
        user.setRoleidRole(roleC.findRole(1));
        user.setUsercreateTime(new Date());
       SecureRandom secureRandom = new SecureRandom();
      byte[] salt = secureRandom.generateSeed(64);
      int iterations = 10000; // TODO put algorithm and seed size into settings
      user.setUserpasswordSalt(salt);
      user.setUsersaltIteration(iterations);
        Sha512Hash passwordHash = new Sha512Hash(user.getUserpassword(), salt, iterations);
           user.setUserpassword(passwordHash.toHex());
           String uniqueId = new Sha512Hash(user.getUserusername(),salt, iterations).toHex();
           user.setUserconfirmed(false);
           user.setLoggedIn(false);
           user.setUseruniqueId(uniqueId);
           //create unique id from username (id is hash of username)
           
           
           System.out.println("name " + user.getUserusername()+
                " surname "+ user.getUserfirstname()+
                " email " + user.getUseremail() + 
                " username " + user.getUserusername() + 
                " password " + user.getUserpassword()+
                " confirmed " + user.getUserconfirmed()+
                    " create_time " + user.getUsercreateTime()+
               " role " + user.getRoleidRole());
         
        try {
            userC.create(user);
              ChatRoomUsers chatRoomLink = new ChatRoomUsers();
           chatRoomLink.setChatRoomUsersJoinedDate(new Date());
           chatRoomLink.setChatRoomUsersconfirmed(true);
           chatRoomLink.setChatRoomidChatRoom(roomJpa.findChatRoom(1));
           
          user =  userC.findByUserUniqueId(user.getUseruniqueId());
          chatRoomLink.setUseruserId(user);
           roomUsersJpa.create(chatRoomLink);
            ConfirmationMail mail = new ConfirmationMail();
            mail.setData(user.getUseremail(), user.getUserusername(), confirmationUrl+user.getUseruniqueId());
            mail.sendMail();
            FacesContext.getCurrentInstance().getExternalContext().redirect("registrationSuccesful.xhtml");
            
        } catch (Exception ex) {
            Logger.getLogger(RegisterBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void handleFileUpload(FileUploadEvent evt) {
        String folderPath = Configuration.folderPath ;
        folderPath = folderPath.replaceAll(" ", "");
        File theFile = new File(folderPath);
        
        theFile.mkdirs();
        try {
            ioc.uploadFile(evt.getFile().getInputstream(), folderPath+evt.getFile().getFileName());
            user.setUserimagePath(evt.getFile().getFileName());
        } catch (IOException ex) {
            Logger.getLogger(RegisterBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public String onFlowProcess(FlowEvent event) {
      System.out.println("Step: " + event.getNewStep());
            return event.getNewStep();
        
    }

    public User getUser() {
        return user;
    }

   

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
