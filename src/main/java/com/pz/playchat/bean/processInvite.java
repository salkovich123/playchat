/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.ChatRoomUsersJpaController;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.mail.InvitationSuccess;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import javax.inject.Named;

/**
 *
 * @author Marcelino
 */
@Named(value = "procesInvite")
@RequestScoped
public class processInvite implements Serializable {
    
    @EJB
    ChatRoomUsersJpaController roomUsersJpa;
    @EJB
    ChatRoomJpaController roomJpa;
    
 
    private String user;
    

    private String room;
            
    
               
     @PostConstruct
       public void init(){
           	FacesContext fc = FacesContext.getCurrentInstance();
           Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
          user =  params.get("user");
          room = params.get("room");
          
           System.out.println("IDS  are USER: " + user + "Room "+ room);
          System.out.println("Starting confirmation checking: ");
         ChatRoomUsers invitationInstance = roomUsersJpa.confirmInvitation(user, room);
                  
                        try {
                      invitationInstance.setChatRoomUsersJoinedDate(new Date());
                      invitationInstance.setChatRoomUsersconfirmed(true);
                     
                       InvitationSuccess mail = new InvitationSuccess();
                     
                            roomUsersJpa.edit(invitationInstance);
                           mail.setData(invitationInstance.getUseruserId().getUseremail(), invitationInstance.getUseruserId().getUserusername(), invitationInstance.getChatRoomidChatRoom().getChatRoomname());
                           mail.sendMail();
                             valid = true;
                       } catch (Exception ex) {
                             valid = false;
                           Logger.getLogger(processInvite.class.getName()).log(Level.SEVERE, null, ex);
                       }
                    
                      
                  
       }

       
    private boolean valid;   

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
       
    

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
       
}
