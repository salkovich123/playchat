/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.AESController;
import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.ChatRoomUsersJpaController;
import com.pz.playchat.chat.controller.IOController;
import com.pz.playchat.chat.controller.KeysJpaController;
import com.pz.playchat.chat.controller.MessagesChatRoomJpaController;
import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.Keys;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import com.pz.playchat.chat.entity.User;
import com.pz.playchat.shiro.Configuration;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Marcelino
 */
@Named(value = "roomBean")
@ViewScoped
public class RoomBean implements Serializable {
    
    @EJB 
    IOController ioc;
    @EJB
    AESController aesC;
    @EJB
    ChatRoomJpaController roomJpa;
    @EJB
    UserJpaController userC;
    @EJB
    KeysJpaController keysJpa;
    @EJB
    ChatRoomUsersJpaController croomUsersJpa;
    @EJB 
    MessagesChatRoomJpaController messJpa;
    
    private ChatRoom room;
    private ChatRoomUsers croomUsers;
    
    String folderPath;
    @PostConstruct
   public void init(){
       folderPath = Configuration.folderPath;
       room = new ChatRoom();
      
   }
   
   public void saveRoom(){
     System.out.println("IN save method");
        try {
            System.out.println("77");
             SecureRandom secureRandom = new SecureRandom();
             System.out.println("79");
      byte[] salt = secureRandom.generateSeed(64);
       System.out.println("81");
         int iterations = 10000; 
         System.out.println("83");
         Sha512Hash passwordHash = new Sha512Hash(room.getChatRoomname(), salt, iterations);
          System.out.println("85");
         room.setChatRoomiterations(iterations);
          System.out.println("87");
           room.setChatRoomsalt(salt);
            System.out.println("89");
           room.setChatRoomUniqueId(passwordHash.toHex());
            System.out.println("91");
            room.setChatRoomcreatedDate(new Date());
             System.out.println("93");
            roomJpa.create(room);
             System.out.println("95");
            System.out.println("Room saved");
            User user ;
            long id = (long)SecurityUtils.getSubject().getPrincipal();
            user = userC.findUser((int) id);
            System.out.println("Setting user to room");
            croomUsers = new ChatRoomUsers();
            croomUsers.setChatRoomUsersJoinedDate(new Date());
            croomUsers.setChatRoomUsersconfirmed(true);
            croomUsers.setChatRoomidChatRoom(room);
            //generate unique id from name of room
        
      
            croomUsers.setUseruserId(user);
            croomUsersJpa.create(croomUsers);
            
            //generate first key for aes encryption
            aesC.generateKey(room);
            System.out.println("Before redirect");
             FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } catch (RollbackFailureException ex) {
            System.out.println("Error" +ex.getMessage());
            Logger.getLogger(RoomBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            System.out.println("Error" +ex.getMessage());
            Logger.getLogger(RoomBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       System.out.println("End save method");
   }
   public String findLastMessage(ChatRoomUsers selectedRoom){
       try{
       MessagesChatRoom mess = messJpa.findLastMessage(selectedRoom.getChatRoomidChatRoom());
     return aesC.decrypt(mess, selectedRoom.getChatRoomidChatRoom());
       }catch(Exception e){
           System.out.println("Error at finding last message in roombean   " + e.getMessage());
           return "No messages";
       }
   }
    public String findLastMessageUser(ChatRoomUsers selectedRoom){
       try{
       MessagesChatRoom mess = messJpa.findLastMessage(selectedRoom.getChatRoomidChatRoom());
     return mess.getUseruserId().getUserusername();
       }catch(Exception e){
           System.out.println("Error at finding last message in roombean   " + e.getMessage());
           return "No Messages!";
       }
   }
   public void handleFileUpload(FileUploadEvent evt) {
        String folderPath = Configuration.folderPath+"rooms/" ;
        folderPath = folderPath.replaceAll(" ", "");
        File theFile = new File(folderPath);
        
        theFile.mkdirs();
        try {
            System.out.println(evt.toString());
            System.out.println("File name: " + evt.getFile().getFileName() +" path " + folderPath+evt.getFile().getFileName() );
            ioc.uploadFile(evt.getFile().getInputstream(), folderPath+evt.getFile().getFileName());
            room.setChatRoomimage("rooms/"+evt.getFile().getFileName());
           
        } catch (IOException ex) {
            Logger.getLogger(RoomBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   public void getLink(ChatRoomUsers room) throws IOException{
     
       FacesContext.getCurrentInstance().getExternalContext().redirect("chatRoom.xhtml");
       
   }
    public ChatRoom getRoom() {
        return room;
    }

    public void setRoom(ChatRoom room) {
        this.room = room;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
   
   
    
}
