/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.entity.User;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author Marcelino
 */
@Named(value="menuBean")
@ViewScoped
public class MenuBean implements Serializable {
    @EJB
    UserJpaController userJpa;
    
    User user;
    @PostConstruct
    public void init(){
        long id =(long) SecurityUtils.getSubject().getPrincipal();
        user = userJpa.findUser((int)id);
    }
    
    public void linkCreateRoom(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("createRoom.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void linkMyRooms(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("myRooms.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void linkIndex(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MenuBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
