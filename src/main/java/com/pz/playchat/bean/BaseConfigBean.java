/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.shiro.Configuration;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author Marcelino
 */
@Named(value = "baseConfigBean")
@ApplicationScoped
public class BaseConfigBean {
    // bean for Setting path of the server
    
    String serverPath;
    String folderPath;
    
    @PostConstruct
    public void init(){
        serverPath = Configuration.serverPath;
        folderPath = Configuration.folderPath;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
    
    
}
