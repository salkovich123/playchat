/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.AESController;
import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.KeysJpaController;
import com.pz.playchat.chat.controller.MessagesChatRoomJpaController;
import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.Keys;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.util.ByteSource;

/**
 *
 * @author Marcelino
 */
@Named(value = "indexBean")
@ViewScoped
public class IndexBean implements Serializable {
String result;
    @EJB
    private AESController aesC;
    private String temp;
    @EJB
    KeysJpaController keysController;
    @PostConstruct
    public void init() {
        /*
    try {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256);
        SecretKey secretKey = keyGen.generateKey();
        ByteSource resultenc = aesC.encryptTest("Seos", secretKey.getEncoded());
        aesC.decryptTest(resultenc.getBytes(), secretKey.getEncoded());
    } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, ex);
    }
        
        */
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    
    
}
