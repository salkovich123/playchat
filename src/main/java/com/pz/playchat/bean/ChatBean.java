/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.AESController;
import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.ChatRoomUsersJpaController;
import com.pz.playchat.chat.controller.KeysJpaController;
import com.pz.playchat.chat.controller.MessagesChatRoomJpaController;
import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.Keys;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import com.pz.playchat.chat.entity.User;
import com.pz.playchat.mail.BannedMail;
import com.pz.playchat.mail.InvitationMail;
import com.pz.playchat.shiro.Configuration;
import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.AesCipherService;
import org.omnifaces.util.Faces;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 *
 * @author Marcelino
 */
@Named(value = "chatBean")
@ViewScoped
public class ChatBean implements Serializable {
    

   @EJB
   KeysJpaController keysJpa;
   @EJB
   AESController aesC;
    @EJB
    UserJpaController userJpa;
    @EJB
    ChatRoomJpaController roomJpa;
    @EJB
    ChatRoomUsersJpaController chatRoomUsers;
    @EJB
    MessagesChatRoomJpaController messJpa;
   
    //lists
    private List<User> users;
    private List<User> allUsers;
    private List<User> selectedUsers;
    private List<User> bannedUsers;
    private List<MessagesChatRoom> messages;
    //variables
    private String name;
     private String roomName;
     private String picPath;
    private String message;
    private String folderPath;
    private String url;
    private User loggedInUser;
  
    public void init(){
    users = new ArrayList<User>();
    bannedUsers =  new ArrayList<User>();
    folderPath = Configuration.folderPath;
   System.out.println("Loaded chat   name is " +name + "RoomName" + roomName  ) ;
 
  messages = messJpa.findMessagesByRoom(roomJpa.findChatRoomByUniqueId(roomName));
  System.out.println("Messages to display:    " + messages.size());
   picPath = Configuration.folderPath;
   allUsers = userJpa.findActiveUsers();
  url = Configuration.serverPath;
  long id = (long) SecurityUtils.getSubject().getPrincipal();
  User user  = userJpa.findUser((int)id);
  loggedInUser = userJpa.findUser((int)id);
  user.setLoggedIn(true);
    
       try {
           userJpa.edit(user);
       } catch (RollbackFailureException ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (Exception ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       }
       
       
       users = chatRoomUsers.ChatRoomUsers(roomJpa.findChatRoomByUniqueId(roomName));
  EventBus bus = EventBusFactory.getDefault().eventBus();
  
           bus.publish("/activity","");
     }
    public List<User> nonJoinedUsers(){
    System.out.println("Selecting non-joined users");
    allUsers = userJpa.findActiveUsers();
        allUsers.removeAll(users);
        return allUsers;
    }
     
    public void deleteUserFromChat(){
        System.out.println("todo: delete user" + bannedUsers.get(0).getUserusername());
       
       ChatRoom room =  roomJpa.findChatRoomByUniqueId(roomName);
          List<ChatRoomUsers> userschat = chatRoomUsers.getChatRoomUsersByRoom(roomJpa.findChatRoomByUniqueId(roomName));
          for(int j=0;j<userschat.size();j++){
        for(int i = 0;i< bannedUsers.size();i++){
         
                try {
                      BannedMail m = new BannedMail();
                    m.setData(userschat.get(i).getUseruserId().getUseremail(), userschat.get(i).getUseruserId().getUserusername(), room.getChatRoomname() , loggedInUser.getUserusername());
                    m.sendMail();
                    System.out.println("User that is being destroyed:   " + userschat.get(i).getUseruserId().getUserusername());
                  ChatRoomUsers tempDelete =   chatRoomUsers.findUserInChatRoom(bannedUsers.get(i), roomJpa.findChatRoomByUniqueId(roomName));
                    chatRoomUsers.destroy(tempDelete.getChatRoomUsersId());
                   
           
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        
          }
       EventBus bus = EventBusFactory.getDefault().eventBus();
  
           bus.publish("/delete","");
        
    }
    public void checkUserPermitted(){
        System.out.println("in check user if he is permited!");
        long id = (long) SecurityUtils.getSubject().getPrincipal();
        User user = userJpa.findUser((int)id);
       try{
        chatRoomUsers.findUserInChatRoom(user, roomJpa.findChatRoomByUniqueId(roomName));
        System.out.println("User in group");
        }catch(Exception e){
            try {
                System.out.println("User not in group... redirecting");
                FacesContext.getCurrentInstance().getExternalContext().redirect("myRooms.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        users = chatRoomUsers.ChatRoomUsers(roomJpa.findChatRoomByUniqueId(roomName));
        nonJoinedUsers();
           
    }
    public void destroy(){
        System.out.println("Destroying logging out user");
        long id = (long)SecurityUtils.getSubject().getPrincipal();
        User usr = userJpa.findUser((int) id);
        usr.setLoggedIn(false);
       try {
           userJpa.edit(usr);
           System.out.println("User logged out");
           
           EventBus bus = EventBusFactory.getDefault().eventBus();
  
           bus.publish("/activity","");
       } catch (RollbackFailureException ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (Exception ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       }
       
    }
    
    
    public void loadChat(){
        System.out.println("loadChat");
    }
    public void checkUserActive(){
      System.out.println("Properly pushed");
        users = chatRoomUsers.ChatRoomUsers(roomJpa.findChatRoomByUniqueId(roomName));
    }
    //method for sending message
    //1. encrypt message with aes
    //2. save message to database
    //3. sendPush update
    public void sendMessage(){
       try {
           System.out.println("1.st part");
           System.out.println("Message is: " + message);
           byte[] encrypted = aesC.encrypt(message,roomJpa.findChatRoomByUniqueId(roomName));
           System.out.println("encrypted size:" + encrypted.length );
           
            System.out.println("Second part - saving to database" );
           MessagesChatRoom mess = new MessagesChatRoom();
           mess.setChatRoomidChatRoom(roomJpa.findChatRoomByUniqueId(roomName));
           mess.setMessagescontent(encrypted);
           mess.setMessagescreatedDate(new Date());
           long id = (long) SecurityUtils.getSubject().getPrincipal();
           mess.setUseruserId(userJpa.findUser((int)id));
           messJpa.create(mess);
           System.out.println("End saving message!");
           messages = messJpa.findMessagesByRoom(roomJpa.findChatRoomByUniqueId(roomName));
           message = "";
           //stage 3 push connection
           System.out.println("Step 3: Push notification!   ");
           EventBus bus = EventBusFactory.getDefault().eventBus();
           bus.publish("/refresh",true);
           
           
       } catch (RollbackFailureException ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (Exception ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       }
        
    }
    public void refreshMessages(){
        System.out.println("Refreshing messages!");
        messages = messJpa.findMessagesByRoom(roomJpa.findChatRoomByUniqueId(roomName));
    }
    public void changeKeys(){
        aesC.generateKey(roomJpa.findChatRoomByUniqueId(roomName));
   
    }
    public String decryptMessage (MessagesChatRoom mess){
       return aesC.decrypt(mess, roomJpa.findChatRoomByUniqueId(roomName));
    }
    public void sendInvite(){
        System.out.println("SelectedUsers size: " + selectedUsers.size());
        for(int i = 0;i<selectedUsers.size();i++){
            try {
                ChatRoomUsers invitation = new ChatRoomUsers();
                invitation.setChatRoomUsersconfirmed(false);
                invitation.setChatRoomidChatRoom(roomJpa.findChatRoomByUniqueId(roomName));
                invitation.setUseruserId( selectedUsers.get(i));
                chatRoomUsers.create(invitation);
                
                
                InvitationMail mail = new InvitationMail();
                mail.setData(selectedUsers.get(i).getUseremail(), roomJpa.getChatRoomHolder(roomJpa.findChatRoomByUniqueId(roomName)).getUserusername(), selectedUsers.get(i).getUserusername(), generateUrl(selectedUsers.get(i).getUseruniqueId(),roomJpa.findChatRoomByUniqueId(roomName).getChatRoomUniqueId()) ,roomJpa.findChatRoomByUniqueId(roomName).getChatRoomname());
                mail.sendMail();
            } catch (Exception ex) {
                Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public String padValue(int value){
        if(value<10){
            return "0"+value;
        }else{
            return ""+value;
        }
    }
    public void redirectToRoom(){
       try {System.out.println("Redirecting");
           FacesContext.getCurrentInstance().getExternalContext().redirect(Configuration.serverPath+"users/chatRoom.xhtml?roomName="+roomName);
       } catch (IOException ex) {
           Logger.getLogger(ChatBean.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    public boolean checkDate(Date date){
        Date dateToday  = new Date();
                dateToday.setHours(0);
                dateToday.setMinutes(0);
       return date.after(dateToday);
       /*   Date dateOrg = date;
       dateOrg.setHours(0);
       dateOrg.setMinutes(0);
       dateOrg.setSeconds(0);
       System.out.println(date);
       //today
       if(new Date().after(date)){
       System.out.println("method true return");
       return true;
       }
       else{
       System.out.println("method false return");
       return false;
       }*/
     
    }
    public String generateUrl(String user, String room){
       return Configuration.serverPath+"processInvite.xhtml?user="+user+"&room="+room;
    }
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int randomize(){
       double i = Math.random()*30;
       System.out.println("rANDOMIZE " + i);
       return (int) i;
    }
    public int randomizeleft(){
       double i = Math.random()*30;
       System.out.println("rANDOMIZE " + i);
       return (int) i;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public List<User> getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(List<User> allUsers) {
        this.allUsers = allUsers;
    }

    public List<User> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<User> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public String getMessage() {
        return message;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MessagesChatRoom> getMessages() {
        return messages;
    }

    public void setMessages(List<MessagesChatRoom> messages) {
        this.messages = messages;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<User> getBannedUsers() {
        return bannedUsers;
    }

    public void setBannedUsers(List<User> bannedUsers) {
        this.bannedUsers = bannedUsers;
    }

   

  
    
    
    
}
