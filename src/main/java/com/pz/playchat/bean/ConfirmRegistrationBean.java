/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.bean;

import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.User;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Marcelino
 */
@ManagedBean
@javax.enterprise.context.RequestScoped
public class ConfirmRegistrationBean {
    
    @EJB
    UserJpaController userC;
    
    @ManagedProperty(value="#{param.key}")
    private String key;
    
    private boolean valid;
    
    @PostConstruct
    public void init(){
        System.out.println("Key is: " + key);
        User user = userC.findByUserUniqueId(key);
        System.out.println("User" +  user);
        if(user != null){
            user.setUserconfirmed(true);
            valid=true;
            try {
                userC.edit(user);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(ConfirmRegistrationBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ConfirmRegistrationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            valid = false;
        }
        
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    
    
}
