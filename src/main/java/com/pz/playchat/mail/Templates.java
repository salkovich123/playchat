/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.mail;

import com.pz.playchat.shiro.Configuration;

/**
 *
 * @author Marcelino
 */
public class Templates {
    
    public static String confirmationEmail (String username, String url){
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"<head>\n" +
"	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"	<meta name=\"viewport\" content=\"width=device-width\"/>\n" +
"	<style>\n" +
"    \n" +
"  #outlook a { \n" +
"    padding:0; \n" +
"  } \n" +
"\n" +
"  body{ \n" +
"    width:100% !important; \n" +
"    min-width: 100%;\n" +
"    -webkit-text-size-adjust:100%; \n" +
"    -ms-text-size-adjust:100%; \n" +
"    margin:0; \n" +
"    padding:0;\n" +
"  }\n" +
"\n" +
"  .ExternalClass { \n" +
"    width:100%;\n" +
"  } \n" +
"\n" +
"  .ExternalClass, \n" +
"  .ExternalClass p, \n" +
"  .ExternalClass span, \n" +
"  .ExternalClass font, \n" +
"  .ExternalClass td, \n" +
"  .ExternalClass div { \n" +
"    line-height: 100%; \n" +
"  } \n" +
"\n" +
"  #backgroundTable { \n" +
"    margin:0; \n" +
"    padding:0; \n" +
"    width:100% !important; \n" +
"    line-height: 100% !important; \n" +
"  }\n" +
"\n" +
"  img { \n" +
"    outline:none; \n" +
"    text-decoration:none; \n" +
"    -ms-interpolation-mode: bicubic;\n" +
"    width: auto;\n" +
"    max-width: 100%; \n" +
"    float: left; \n" +
"    clear: both; \n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  center {\n" +
"    width: 100%;\n" +
"    min-width: 580px;\n" +
"  }\n" +
"\n" +
"  a img { \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  p {\n" +
"    margin: 0 0 0 10px;\n" +
"  }\n" +
"\n" +
"  table {\n" +
"    border-spacing: 0;\n" +
"    border-collapse: collapse;\n" +
"  }\n" +
"\n" +
"  td { \n" +
"    word-break: break-word;\n" +
"    -webkit-hyphens: auto;\n" +
"    -moz-hyphens: auto;\n" +
"    hyphens: auto;\n" +
"    border-collapse: collapse !important; \n" +
"  }\n" +
"\n" +
"  table, tr, td {\n" +
"    padding: 0;\n" +
"    vertical-align: top;\n" +
"    text-align: left;\n" +
"  }\n" +
"\n" +
"  hr {\n" +
"    color: #d9d9d9; \n" +
"    background-color: #d9d9d9; \n" +
"    height: 1px; \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  /* Responsive Grid */\n" +
"\n" +
"  table.body {\n" +
"    height: 100%;\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  table.container {\n" +
"    width: 580px;\n" +
"    margin: 0 auto;\n" +
"    text-align: inherit;\n" +
"  }\n" +
"\n" +
"  table.row { \n" +
"    padding: 0px; \n" +
"    width: 100%;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.container table.row {\n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  td.wrapper {\n" +
"    padding: 10px 20px 0px 0px;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.columns,\n" +
"  table.column {\n" +
"    margin: 0 auto;\n" +
"  }\n" +
"\n" +
"  table.columns td,\n" +
"  table.column td {\n" +
"    padding: 0px 0px 10px; \n" +
"  }\n" +
"\n" +
"  table.columns td.sub-columns,\n" +
"  table.column td.sub-columns,\n" +
"  table.columns td.sub-column,\n" +
"  table.column td.sub-column {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  td.sub-column, td.sub-columns {\n" +
"    min-width: 0px;\n" +
"  }\n" +
"\n" +
"  table.row td.last,\n" +
"  table.container td.last {\n" +
"    padding-right: 0px;\n" +
"  }\n" +
"\n" +
"  table.one { width: 30px; }\n" +
"  table.two { width: 80px; }\n" +
"  table.three { width: 130px; }\n" +
"  table.four { width: 180px; }\n" +
"  table.five { width: 230px; }\n" +
"  table.six { width: 280px; }\n" +
"  table.seven { width: 330px; }\n" +
"  table.eight { width: 380px; }\n" +
"  table.nine { width: 430px; }\n" +
"  table.ten { width: 480px; }\n" +
"  table.eleven { width: 530px; }\n" +
"  table.twelve { width: 580px; }\n" +
"\n" +
"  table.one center { min-width: 30px; }\n" +
"  table.two center { min-width: 80px; }\n" +
"  table.three center { min-width: 130px; }\n" +
"  table.four center { min-width: 180px; }\n" +
"  table.five center { min-width: 230px; }\n" +
"  table.six center { min-width: 280px; }\n" +
"  table.seven center { min-width: 330px; }\n" +
"  table.eight center { min-width: 380px; }\n" +
"  table.nine center { min-width: 430px; }\n" +
"  table.ten center { min-width: 480px; }\n" +
"  table.eleven center { min-width: 530px; }\n" +
"  table.twelve center { min-width: 580px; }\n" +
"\n" +
"  table.one .panel center { min-width: 10px; }\n" +
"  table.two .panel center { min-width: 60px; }\n" +
"  table.three .panel center { min-width: 110px; }\n" +
"  table.four .panel center { min-width: 160px; }\n" +
"  table.five .panel center { min-width: 210px; }\n" +
"  table.six .panel center { min-width: 260px; }\n" +
"  table.seven .panel center { min-width: 310px; }\n" +
"  table.eight .panel center { min-width: 360px; }\n" +
"  table.nine .panel center { min-width: 410px; }\n" +
"  table.ten .panel center { min-width: 460px; }\n" +
"  table.eleven .panel center { min-width: 510px; }\n" +
"  table.twelve .panel center { min-width: 560px; }\n" +
"\n" +
"  .body .columns td.one,\n" +
"  .body .column td.one { width: 8.333333%; }\n" +
"  .body .columns td.two,\n" +
"  .body .column td.two { width: 16.666666%; }\n" +
"  .body .columns td.three,\n" +
"  .body .column td.three { width: 25%; }\n" +
"  .body .columns td.four,\n" +
"  .body .column td.four { width: 33.333333%; }\n" +
"  .body .columns td.five,\n" +
"  .body .column td.five { width: 41.666666%; }\n" +
"  .body .columns td.six,\n" +
"  .body .column td.six { width: 50%; }\n" +
"  .body .columns td.seven,\n" +
"  .body .column td.seven { width: 58.333333%; }\n" +
"  .body .columns td.eight,\n" +
"  .body .column td.eight { width: 66.666666%; }\n" +
"  .body .columns td.nine,\n" +
"  .body .column td.nine { width: 75%; }\n" +
"  .body .columns td.ten,\n" +
"  .body .column td.ten { width: 83.333333%; }\n" +
"  .body .columns td.eleven,\n" +
"  .body .column td.eleven { width: 91.666666%; }\n" +
"  .body .columns td.twelve,\n" +
"  .body .column td.twelve { width: 100%; }\n" +
"\n" +
"  td.offset-by-one { padding-left: 50px; }\n" +
"  td.offset-by-two { padding-left: 100px; }\n" +
"  td.offset-by-three { padding-left: 150px; }\n" +
"  td.offset-by-four { padding-left: 200px; }\n" +
"  td.offset-by-five { padding-left: 250px; }\n" +
"  td.offset-by-six { padding-left: 300px; }\n" +
"  td.offset-by-seven { padding-left: 350px; }\n" +
"  td.offset-by-eight { padding-left: 400px; }\n" +
"  td.offset-by-nine { padding-left: 450px; }\n" +
"  td.offset-by-ten { padding-left: 500px; }\n" +
"  td.offset-by-eleven { padding-left: 550px; }\n" +
"\n" +
"  td.expander {\n" +
"    visibility: hidden;\n" +
"    width: 0px;\n" +
"    padding: 0 !important;\n" +
"  }\n" +
"\n" +
"  table.columns .text-pad,\n" +
"  table.column .text-pad {\n" +
"    padding-left: 10px;\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .left-text-pad,\n" +
"  table.columns .text-pad-left,\n" +
"  table.column .left-text-pad,\n" +
"  table.column .text-pad-left {\n" +
"    padding-left: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .right-text-pad,\n" +
"  table.columns .text-pad-right,\n" +
"  table.column .right-text-pad,\n" +
"  table.column .text-pad-right {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  /* Block Grid */\n" +
"\n" +
"  .block-grid {\n" +
"    width: 100%;\n" +
"    max-width: 580px;\n" +
"  }\n" +
"\n" +
"  .block-grid td {\n" +
"    display: inline-block;\n" +
"    padding:10px;\n" +
"  }\n" +
"\n" +
"  .two-up td {\n" +
"    width:270px;\n" +
"  }\n" +
"\n" +
"  .three-up td {\n" +
"    width:173px;\n" +
"  }\n" +
"\n" +
"  .four-up td {\n" +
"    width:125px;\n" +
"  }\n" +
"\n" +
"  .five-up td {\n" +
"    width:96px;\n" +
"  }\n" +
"\n" +
"  .six-up td {\n" +
"    width:76px;\n" +
"  }\n" +
"\n" +
"  .seven-up td {\n" +
"    width:62px;\n" +
"  }\n" +
"\n" +
"  .eight-up td {\n" +
"    width:52px;\n" +
"  }\n" +
"\n" +
"  /* Alignment & Visibility Classes */\n" +
"\n" +
"  table.center, td.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  h1.center,\n" +
"  h2.center,\n" +
"  h3.center,\n" +
"  h4.center,\n" +
"  h5.center,\n" +
"  h6.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  span.center {\n" +
"    display: block;\n" +
"    width: 100%;\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  img.center {\n" +
"    margin: 0 auto;\n" +
"    float: none;\n" +
"  }\n" +
"\n" +
"  .show-for-small,\n" +
"  .hide-for-desktop {\n" +
"    display: none;\n" +
"  }\n" +
"\n" +
"  /* Typography */\n" +
"\n" +
"  body, table.body, h1, h2, h3, h4, h5, h6, p, td { \n" +
"    color: #222222;\n" +
"    font-family: \"Helvetica\", \"Arial\", sans-serif; \n" +
"    font-weight: normal; \n" +
"    padding:0; \n" +
"    margin: 0;\n" +
"    text-align: left; \n" +
"    line-height: 1.3;\n" +
"  }\n" +
"\n" +
"  h1, h2, h3, h4, h5, h6 {\n" +
"    word-break: normal;\n" +
"  }\n" +
"\n" +
"  h1 {font-size: 40px;}\n" +
"  h2 {font-size: 36px;}\n" +
"  h3 {font-size: 32px;}\n" +
"  h4 {font-size: 28px;}\n" +
"  h5 {font-size: 24px;}\n" +
"  h6 {font-size: 20px;}\n" +
"  body, table.body, p, td {font-size: 14px;line-height:19px;}\n" +
"\n" +
"  p.lead, p.lede, p.leed {\n" +
"    font-size: 18px;\n" +
"    line-height:21px;\n" +
"  }\n" +
"\n" +
"  p { \n" +
"    margin-bottom: 10px;\n" +
"  }\n" +
"\n" +
"  small {\n" +
"    font-size: 10px;\n" +
"  }\n" +
"\n" +
"  a {\n" +
"    color: #2ba6cb; \n" +
"    text-decoration: none;\n" +
"  }\n" +
"\n" +
"  a:hover { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:active { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:visited { \n" +
"    color: #2ba6cb !important;\n" +
"  }\n" +
"\n" +
"  h1 a, \n" +
"  h2 a, \n" +
"  h3 a, \n" +
"  h4 a, \n" +
"  h5 a, \n" +
"  h6 a {\n" +
"    color: #2ba6cb;\n" +
"  }\n" +
"\n" +
"  h1 a:active, \n" +
"  h2 a:active,  \n" +
"  h3 a:active, \n" +
"  h4 a:active, \n" +
"  h5 a:active, \n" +
"  h6 a:active { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  h1 a:visited, \n" +
"  h2 a:visited,  \n" +
"  h3 a:visited, \n" +
"  h4 a:visited, \n" +
"  h5 a:visited, \n" +
"  h6 a:visited { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  /* Panels */\n" +
"\n" +
"  .panel {\n" +
"    background: #f2f2f2;\n" +
"    border: 1px solid #d9d9d9;\n" +
"    padding: 10px !important;\n" +
"  }\n" +
"\n" +
"  .sub-grid table {\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  .sub-grid td.sub-columns {\n" +
"    padding-bottom: 0;\n" +
"  }\n" +
"\n" +
"  /* Buttons */\n" +
"\n" +
"  table.button,\n" +
"  table.tiny-button,\n" +
"  table.small-button,\n" +
"  table.medium-button,\n" +
"  table.large-button {\n" +
"    width: 100%;\n" +
"    overflow: hidden;\n" +
"  }\n" +
"\n" +
"  table.button td,\n" +
"  table.tiny-button td,\n" +
"  table.small-button td,\n" +
"  table.medium-button td,\n" +
"  table.large-button td {\n" +
"    display: block;\n" +
"    width: auto !important;\n" +
"    text-align: center;\n" +
"    background: #2ba6cb;\n" +
"    border: 1px solid #2284a1;\n" +
"    color: #ffffff;\n" +
"    padding: 8px 0;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td {\n" +
"    padding: 5px 0 4px;\n" +
"  }\n" +
"\n" +
"  table.small-button td {\n" +
"    padding: 8px 0 7px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td {\n" +
"    padding: 12px 0 10px;\n" +
"  }\n" +
"\n" +
"  table.large-button td {\n" +
"    padding: 21px 0 18px;\n" +
"  }\n" +
"\n" +
"  table.button td a,\n" +
"  table.tiny-button td a,\n" +
"  table.small-button td a,\n" +
"  table.medium-button td a,\n" +
"  table.large-button td a {\n" +
"    font-weight: bold;\n" +
"    text-decoration: none;\n" +
"    font-family: Helvetica, Arial, sans-serif;\n" +
"    color: #ffffff;\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td a {\n" +
"    font-size: 12px;\n" +
"    font-weight: normal;\n" +
"  }\n" +
"\n" +
"  table.small-button td a {\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td a {\n" +
"    font-size: 20px;\n" +
"  }\n" +
"\n" +
"  table.large-button td a {\n" +
"    font-size: 24px;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.button:visited td,\n" +
"  table.button:active td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:visited td a,\n" +
"  table.button:active td a {\n" +
"    color: #fff !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.tiny-button:hover td,\n" +
"  table.small-button:hover td,\n" +
"  table.medium-button:hover td,\n" +
"  table.large-button:hover td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:active td a,\n" +
"  table.button td a:visited,\n" +
"  table.tiny-button:hover td a,\n" +
"  table.tiny-button:active td a,\n" +
"  table.tiny-button td a:visited,\n" +
"  table.small-button:hover td a,\n" +
"  table.small-button:active td a,\n" +
"  table.small-button td a:visited,\n" +
"  table.medium-button:hover td a,\n" +
"  table.medium-button:active td a,\n" +
"  table.medium-button td a:visited,\n" +
"  table.large-button:hover td a,\n" +
"  table.large-button:active td a,\n" +
"  table.large-button td a:visited {\n" +
"    color: #ffffff !important; \n" +
"  }\n" +
"\n" +
"  table.secondary td {\n" +
"    background: #e9e9e9;\n" +
"    border-color: #d0d0d0;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary td a {\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td {\n" +
"    background: #d0d0d0 !important;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td a,\n" +
"  table.secondary td a:visited,\n" +
"  table.secondary:active td a {\n" +
"    color: #555 !important;\n" +
"  }\n" +
"\n" +
"  table.success td {\n" +
"    background: #5da423;\n" +
"    border-color: #457a1a;\n" +
"  }\n" +
"\n" +
"  table.success:hover td {\n" +
"    background: #457a1a !important;\n" +
"  }\n" +
"\n" +
"  table.alert td {\n" +
"    background: #c60f13;\n" +
"    border-color: #970b0e;\n" +
"  }\n" +
"\n" +
"  table.alert:hover td {\n" +
"    background: #970b0e !important;\n" +
"  }\n" +
"\n" +
"  table.radius td {\n" +
"    -webkit-border-radius: 3px;\n" +
"    -moz-border-radius: 3px;\n" +
"    border-radius: 3px;\n" +
"  }\n" +
"\n" +
"  table.round td {\n" +
"    -webkit-border-radius: 500px;\n" +
"    -moz-border-radius: 500px;\n" +
"    border-radius: 500px;\n" +
"  }\n" +
"\n" +
"  /* Outlook First */\n" +
"\n" +
"  body.outlook p {\n" +
"    display: inline !important;\n" +
"  }\n" +
"\n" +
"  /*  Media Queries */\n" +
"\n" +
"  @media only screen and (max-width: 600px) {\n" +
"\n" +
"    table[class=\"body\"] img {\n" +
"      width: auto !important;\n" +
"      height: auto !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] center {\n" +
"      min-width: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .container {\n" +
"      width: 95% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .row {\n" +
"      width: 100% !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper {\n" +
"      display: block !important;\n" +
"      padding-right: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns,\n" +
"    table[class=\"body\"] .column {\n" +
"      table-layout: fixed !important;\n" +
"      float: none !important;\n" +
"      width: 100% !important;\n" +
"      padding-right: 0px !important;\n" +
"      padding-left: 0px !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper.first .columns,\n" +
"    table[class=\"body\"] .wrapper.first .column {\n" +
"      display: table !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td,\n" +
"    table[class=\"body\"] table.column td {\n" +
"      width: 100% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns td.one,\n" +
"    table[class=\"body\"] .column td.one { width: 8.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.two,\n" +
"    table[class=\"body\"] .column td.two { width: 16.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.three,\n" +
"    table[class=\"body\"] .column td.three { width: 25% !important; }\n" +
"    table[class=\"body\"] .columns td.four,\n" +
"    table[class=\"body\"] .column td.four { width: 33.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.five,\n" +
"    table[class=\"body\"] .column td.five { width: 41.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.six,\n" +
"    table[class=\"body\"] .column td.six { width: 50% !important; }\n" +
"    table[class=\"body\"] .columns td.seven,\n" +
"    table[class=\"body\"] .column td.seven { width: 58.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eight,\n" +
"    table[class=\"body\"] .column td.eight { width: 66.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.nine,\n" +
"    table[class=\"body\"] .column td.nine { width: 75% !important; }\n" +
"    table[class=\"body\"] .columns td.ten,\n" +
"    table[class=\"body\"] .column td.ten { width: 83.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eleven,\n" +
"    table[class=\"body\"] .column td.eleven { width: 91.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.twelve,\n" +
"    table[class=\"body\"] .column td.twelve { width: 100% !important; }\n" +
"\n" +
"    table[class=\"body\"] td.offset-by-one,\n" +
"    table[class=\"body\"] td.offset-by-two,\n" +
"    table[class=\"body\"] td.offset-by-three,\n" +
"    table[class=\"body\"] td.offset-by-four,\n" +
"    table[class=\"body\"] td.offset-by-five,\n" +
"    table[class=\"body\"] td.offset-by-six,\n" +
"    table[class=\"body\"] td.offset-by-seven,\n" +
"    table[class=\"body\"] td.offset-by-eight,\n" +
"    table[class=\"body\"] td.offset-by-nine,\n" +
"    table[class=\"body\"] td.offset-by-ten,\n" +
"    table[class=\"body\"] td.offset-by-eleven {\n" +
"      padding-left: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td.expander {\n" +
"      width: 1px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .right-text-pad,\n" +
"    table[class=\"body\"] .text-pad-right {\n" +
"      padding-left: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .left-text-pad,\n" +
"    table[class=\"body\"] .text-pad-left {\n" +
"      padding-right: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .hide-for-small,\n" +
"    table[class=\"body\"] .show-for-desktop {\n" +
"      display: none !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .show-for-small,\n" +
"    table[class=\"body\"] .hide-for-desktop {\n" +
"      display: inherit !important;\n" +
"    }\n" +
"  }\n" +
"\n" +
"    </style>\n" +
"    <style>\n" +
"\n" +
"      table.facebook td {\n" +
"        background: #3b5998;\n" +
"        border-color: #2d4473;\n" +
"      }\n" +
"\n" +
"      table.facebook:hover td {\n" +
"        background: #2d4473 !important;\n" +
"      }\n" +
"\n" +
"      table.twitter td {\n" +
"        background: #00acee;\n" +
"        border-color: #0087bb;\n" +
"      }\n" +
"\n" +
"      table.twitter:hover td {\n" +
"        background: #0087bb !important;\n" +
"      }\n" +
"\n" +
"      table.google-plus td {\n" +
"        background-color: #DB4A39;\n" +
"        border-color: #CC0000;\n" +
"      }\n" +
"\n" +
"      table.google-plus:hover td {\n" +
"        background: #CC0000 !important;\n" +
"      }\n" +
"\n" +
"      .template-label {\n" +
"        color: #ffffff;\n" +
"        font-weight: bold;\n" +
"        font-size: 11px;\n" +
"      }\n" +
"\n" +
"      .callout .panel {\n" +
"        background: #ECF8FF;\n" +
"        border-color: #b9e5ff;\n" +
"      }\n" +
"\n" +
"      .header {\n" +
"        background: #999999;\n" +
"      }\n" +
"\n" +
"      .footer .wrapper {\n" +
"        background: #ebebeb;\n" +
"      }\n" +
"\n" +
"      .footer h5 {\n" +
"        padding-bottom: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .text-pad {\n" +
"        padding-left: 10px;\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .left-text-pad {\n" +
"        padding-left: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .right-text-pad {\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      @media only screen and (max-width: 600px) {\n" +
"\n" +
"        table[class=\"body\"] .right-text-pad {\n" +
"          padding-left: 10px !important;\n" +
"        }\n" +
"\n" +
"        table[class=\"body\"] .left-text-pad {\n" +
"          padding-right: 10px !important;\n" +
"        }\n" +
"      }\n" +
"  </style>\n" +
"  <style>\n" +
"\n" +
"    table.facebook td {\n" +
"      background: #3b5998;\n" +
"      border-color: #2d4473;\n" +
"    }\n" +
"\n" +
"    table.facebook:hover td {\n" +
"      background: #2d4473 !important;\n" +
"    }\n" +
"\n" +
"    table.twitter td {\n" +
"      background: #00acee;\n" +
"      border-color: #0087bb;\n" +
"    }\n" +
"\n" +
"    table.twitter:hover td {\n" +
"      background: #0087bb !important;\n" +
"    }\n" +
"\n" +
"    table.google-plus td {\n" +
"      background-color: #DB4A39;\n" +
"      border-color: #CC0000;\n" +
"    }\n" +
"\n" +
"    table.google-plus:hover td {\n" +
"      background: #CC0000 !important;\n" +
"    }\n" +
"\n" +
"    .template-label {\n" +
"      color: #ffffff;\n" +
"      font-weight: bold;\n" +
"      font-size: 11px;\n" +
"    }\n" +
"\n" +
"    .callout .wrapper {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"\n" +
"    .callout .panel {\n" +
"      background: #ECF8FF;\n" +
"      border-color: #b9e5ff;\n" +
"    }\n" +
"\n" +
"    .header {\n" +
"      background: #999999;\n" +
"    }\n" +
"\n" +
"    .footer .wrapper {\n" +
"      background: #ebebeb;\n" +
"    }\n" +
"\n" +
"    .footer h5 {\n" +
"      padding-bottom: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .text-pad {\n" +
"      padding-left: 10px;\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .left-text-pad {\n" +
"      padding-left: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .right-text-pad {\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    @media only screen and (max-width: 600px) {\n" +
"\n" +
"      table[class=\"body\"] .right-text-pad {\n" +
"        padding-left: 10px !important;\n" +
"      }\n" +
"\n" +
"      table[class=\"body\"] .left-text-pad {\n" +
"        padding-right: 10px !important;\n" +
"      }\n" +
"    }\n" +
"\n" +
"	</style>\n" +
"</head>\n" +
"<body>\n" +
"	<table class=\"body\">\n" +
"		<tr>\n" +
"			<td class=\"center\" align=\"center\" valign=\"top\">\n" +
"        <center>\n" +
"\n" +
"          <table class=\"row header\">\n" +
"            <tr>\n" +
"              <td class=\"center\" align=\"center\">\n" +
"                <center>\n" +
"\n" +
"                  <table class=\"container\">\n" +
"                    <tr>\n" +
"                      <td class=\"wrapper last\">\n" +
"\n" +
"                        <table class=\"twelve columns\">\n" +
"                          <tr>\n" +
"                            <td class=\"six sub-columns\">\n" +
"                              <h1 style=\"color: #009EC3; text-shadow:1px 1px black;\">PlayChat</h1>\n" +
"                            </td>\n" +
"                            <td class=\"six sub-columns last\" style=\"text-align:right; vertical-align:middle;\">\n" +
"                              <span class=\"template-label\" style=\"font-size: 15px;\">Confirm registration</span>\n" +
"                            </td>\n" +
"                            <td class=\"expander\"></td>\n" +
"                          </tr>\n" +
"                        </table>\n" +
"\n" +
"                      </td>\n" +
"                    </tr>\n" +
"                  </table>\n" +
"\n" +
"                </center>\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"          <table class=\"container\">\n" +
"            <tr>\n" +
"              <td>\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td>\n" +
"                            <h1>Hi, "+username+"</h1>\n" +
"                						<p class=\"lead\">Please confirm your registration</p>\n" +
"                						<p>The account has to be confirmed, before it can be used. To confirm your user please click on the link below.</p>\n" +
"                            <p>Confirmation link: <a href='"+url+"'>"+url+"</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row callout\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row footer\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"left-text-pad\">\n" +
"\n" +
"                            <h5>Connect With Us:</h5>\n" +
"\n" +
"                            <table class=\"tiny-button facebook\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Facebook</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button twitter\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Twitter</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button google-plus\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Google +</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"last right-text-pad\">\n" +
"                            <h5>Contact Info:</h5>\n" +
"                            <p>Phone: 408.341.0600</p>\n" +
"                            <p>Email: <a href=\"mailto:hseldon@trantor.com\">example@playchat.com</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td align=\"center\">\n" +
"                            <center>\n" +
"                              <p style=\"text-align:center;\"><a href=\"#\">Terms</a> | <a href=\"#\">Privacy</a> | <a href=\"#\">Unsubscribe</a></p>\n" +
"                            </center>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"              <!-- container end below -->\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"        </center>\n" +
"			</td>\n" +
"		</tr>\n" +
"	</table>\n" +
"</body>\n" +
"</html>";
    }  
       public static String sendInvite (String username,String invitator, String url,String roomName){
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"<head>\n" +
"	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"	<meta name=\"viewport\" content=\"width=device-width\"/>\n" +
"	<style>\n" +
"    \n" +
"  #outlook a { \n" +
"    padding:0; \n" +
"  } \n" +
"\n" +
"  body{ \n" +
"    width:100% !important; \n" +
"    min-width: 100%;\n" +
"    -webkit-text-size-adjust:100%; \n" +
"    -ms-text-size-adjust:100%; \n" +
"    margin:0; \n" +
"    padding:0;\n" +
"  }\n" +
"\n" +
"  .ExternalClass { \n" +
"    width:100%;\n" +
"  } \n" +
"\n" +
"  .ExternalClass, \n" +
"  .ExternalClass p, \n" +
"  .ExternalClass span, \n" +
"  .ExternalClass font, \n" +
"  .ExternalClass td, \n" +
"  .ExternalClass div { \n" +
"    line-height: 100%; \n" +
"  } \n" +
"\n" +
"  #backgroundTable { \n" +
"    margin:0; \n" +
"    padding:0; \n" +
"    width:100% !important; \n" +
"    line-height: 100% !important; \n" +
"  }\n" +
"\n" +
"  img { \n" +
"    outline:none; \n" +
"    text-decoration:none; \n" +
"    -ms-interpolation-mode: bicubic;\n" +
"    width: auto;\n" +
"    max-width: 100%; \n" +
"    float: left; \n" +
"    clear: both; \n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  center {\n" +
"    width: 100%;\n" +
"    min-width: 580px;\n" +
"  }\n" +
"\n" +
"  a img { \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  p {\n" +
"    margin: 0 0 0 10px;\n" +
"  }\n" +
"\n" +
"  table {\n" +
"    border-spacing: 0;\n" +
"    border-collapse: collapse;\n" +
"  }\n" +
"\n" +
"  td { \n" +
"    word-break: break-word;\n" +
"    -webkit-hyphens: auto;\n" +
"    -moz-hyphens: auto;\n" +
"    hyphens: auto;\n" +
"    border-collapse: collapse !important; \n" +
"  }\n" +
"\n" +
"  table, tr, td {\n" +
"    padding: 0;\n" +
"    vertical-align: top;\n" +
"    text-align: left;\n" +
"  }\n" +
"\n" +
"  hr {\n" +
"    color: #d9d9d9; \n" +
"    background-color: #d9d9d9; \n" +
"    height: 1px; \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  /* Responsive Grid */\n" +
"\n" +
"  table.body {\n" +
"    height: 100%;\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  table.container {\n" +
"    width: 580px;\n" +
"    margin: 0 auto;\n" +
"    text-align: inherit;\n" +
"  }\n" +
"\n" +
"  table.row { \n" +
"    padding: 0px; \n" +
"    width: 100%;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.container table.row {\n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  td.wrapper {\n" +
"    padding: 10px 20px 0px 0px;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.columns,\n" +
"  table.column {\n" +
"    margin: 0 auto;\n" +
"  }\n" +
"\n" +
"  table.columns td,\n" +
"  table.column td {\n" +
"    padding: 0px 0px 10px; \n" +
"  }\n" +
"\n" +
"  table.columns td.sub-columns,\n" +
"  table.column td.sub-columns,\n" +
"  table.columns td.sub-column,\n" +
"  table.column td.sub-column {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  td.sub-column, td.sub-columns {\n" +
"    min-width: 0px;\n" +
"  }\n" +
"\n" +
"  table.row td.last,\n" +
"  table.container td.last {\n" +
"    padding-right: 0px;\n" +
"  }\n" +
"\n" +
"  table.one { width: 30px; }\n" +
"  table.two { width: 80px; }\n" +
"  table.three { width: 130px; }\n" +
"  table.four { width: 180px; }\n" +
"  table.five { width: 230px; }\n" +
"  table.six { width: 280px; }\n" +
"  table.seven { width: 330px; }\n" +
"  table.eight { width: 380px; }\n" +
"  table.nine { width: 430px; }\n" +
"  table.ten { width: 480px; }\n" +
"  table.eleven { width: 530px; }\n" +
"  table.twelve { width: 580px; }\n" +
"\n" +
"  table.one center { min-width: 30px; }\n" +
"  table.two center { min-width: 80px; }\n" +
"  table.three center { min-width: 130px; }\n" +
"  table.four center { min-width: 180px; }\n" +
"  table.five center { min-width: 230px; }\n" +
"  table.six center { min-width: 280px; }\n" +
"  table.seven center { min-width: 330px; }\n" +
"  table.eight center { min-width: 380px; }\n" +
"  table.nine center { min-width: 430px; }\n" +
"  table.ten center { min-width: 480px; }\n" +
"  table.eleven center { min-width: 530px; }\n" +
"  table.twelve center { min-width: 580px; }\n" +
"\n" +
"  table.one .panel center { min-width: 10px; }\n" +
"  table.two .panel center { min-width: 60px; }\n" +
"  table.three .panel center { min-width: 110px; }\n" +
"  table.four .panel center { min-width: 160px; }\n" +
"  table.five .panel center { min-width: 210px; }\n" +
"  table.six .panel center { min-width: 260px; }\n" +
"  table.seven .panel center { min-width: 310px; }\n" +
"  table.eight .panel center { min-width: 360px; }\n" +
"  table.nine .panel center { min-width: 410px; }\n" +
"  table.ten .panel center { min-width: 460px; }\n" +
"  table.eleven .panel center { min-width: 510px; }\n" +
"  table.twelve .panel center { min-width: 560px; }\n" +
"\n" +
"  .body .columns td.one,\n" +
"  .body .column td.one { width: 8.333333%; }\n" +
"  .body .columns td.two,\n" +
"  .body .column td.two { width: 16.666666%; }\n" +
"  .body .columns td.three,\n" +
"  .body .column td.three { width: 25%; }\n" +
"  .body .columns td.four,\n" +
"  .body .column td.four { width: 33.333333%; }\n" +
"  .body .columns td.five,\n" +
"  .body .column td.five { width: 41.666666%; }\n" +
"  .body .columns td.six,\n" +
"  .body .column td.six { width: 50%; }\n" +
"  .body .columns td.seven,\n" +
"  .body .column td.seven { width: 58.333333%; }\n" +
"  .body .columns td.eight,\n" +
"  .body .column td.eight { width: 66.666666%; }\n" +
"  .body .columns td.nine,\n" +
"  .body .column td.nine { width: 75%; }\n" +
"  .body .columns td.ten,\n" +
"  .body .column td.ten { width: 83.333333%; }\n" +
"  .body .columns td.eleven,\n" +
"  .body .column td.eleven { width: 91.666666%; }\n" +
"  .body .columns td.twelve,\n" +
"  .body .column td.twelve { width: 100%; }\n" +
"\n" +
"  td.offset-by-one { padding-left: 50px; }\n" +
"  td.offset-by-two { padding-left: 100px; }\n" +
"  td.offset-by-three { padding-left: 150px; }\n" +
"  td.offset-by-four { padding-left: 200px; }\n" +
"  td.offset-by-five { padding-left: 250px; }\n" +
"  td.offset-by-six { padding-left: 300px; }\n" +
"  td.offset-by-seven { padding-left: 350px; }\n" +
"  td.offset-by-eight { padding-left: 400px; }\n" +
"  td.offset-by-nine { padding-left: 450px; }\n" +
"  td.offset-by-ten { padding-left: 500px; }\n" +
"  td.offset-by-eleven { padding-left: 550px; }\n" +
"\n" +
"  td.expander {\n" +
"    visibility: hidden;\n" +
"    width: 0px;\n" +
"    padding: 0 !important;\n" +
"  }\n" +
"\n" +
"  table.columns .text-pad,\n" +
"  table.column .text-pad {\n" +
"    padding-left: 10px;\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .left-text-pad,\n" +
"  table.columns .text-pad-left,\n" +
"  table.column .left-text-pad,\n" +
"  table.column .text-pad-left {\n" +
"    padding-left: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .right-text-pad,\n" +
"  table.columns .text-pad-right,\n" +
"  table.column .right-text-pad,\n" +
"  table.column .text-pad-right {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  /* Block Grid */\n" +
"\n" +
"  .block-grid {\n" +
"    width: 100%;\n" +
"    max-width: 580px;\n" +
"  }\n" +
"\n" +
"  .block-grid td {\n" +
"    display: inline-block;\n" +
"    padding:10px;\n" +
"  }\n" +
"\n" +
"  .two-up td {\n" +
"    width:270px;\n" +
"  }\n" +
"\n" +
"  .three-up td {\n" +
"    width:173px;\n" +
"  }\n" +
"\n" +
"  .four-up td {\n" +
"    width:125px;\n" +
"  }\n" +
"\n" +
"  .five-up td {\n" +
"    width:96px;\n" +
"  }\n" +
"\n" +
"  .six-up td {\n" +
"    width:76px;\n" +
"  }\n" +
"\n" +
"  .seven-up td {\n" +
"    width:62px;\n" +
"  }\n" +
"\n" +
"  .eight-up td {\n" +
"    width:52px;\n" +
"  }\n" +
"\n" +
"  /* Alignment & Visibility Classes */\n" +
"\n" +
"  table.center, td.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  h1.center,\n" +
"  h2.center,\n" +
"  h3.center,\n" +
"  h4.center,\n" +
"  h5.center,\n" +
"  h6.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  span.center {\n" +
"    display: block;\n" +
"    width: 100%;\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  img.center {\n" +
"    margin: 0 auto;\n" +
"    float: none;\n" +
"  }\n" +
"\n" +
"  .show-for-small,\n" +
"  .hide-for-desktop {\n" +
"    display: none;\n" +
"  }\n" +
"\n" +
"  /* Typography */\n" +
"\n" +
"  body, table.body, h1, h2, h3, h4, h5, h6, p, td { \n" +
"    color: #222222;\n" +
"    font-family: \"Helvetica\", \"Arial\", sans-serif; \n" +
"    font-weight: normal; \n" +
"    padding:0; \n" +
"    margin: 0;\n" +
"    text-align: left; \n" +
"    line-height: 1.3;\n" +
"  }\n" +
"\n" +
"  h1, h2, h3, h4, h5, h6 {\n" +
"    word-break: normal;\n" +
"  }\n" +
"\n" +
"  h1 {font-size: 40px;}\n" +
"  h2 {font-size: 36px;}\n" +
"  h3 {font-size: 32px;}\n" +
"  h4 {font-size: 28px;}\n" +
"  h5 {font-size: 24px;}\n" +
"  h6 {font-size: 20px;}\n" +
"  body, table.body, p, td {font-size: 14px;line-height:19px;}\n" +
"\n" +
"  p.lead, p.lede, p.leed {\n" +
"    font-size: 18px;\n" +
"    line-height:21px;\n" +
"  }\n" +
"\n" +
"  p { \n" +
"    margin-bottom: 10px;\n" +
"  }\n" +
"\n" +
"  small {\n" +
"    font-size: 10px;\n" +
"  }\n" +
"\n" +
"  a {\n" +
"    color: #2ba6cb; \n" +
"    text-decoration: none;\n" +
"  }\n" +
"\n" +
"  a:hover { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:active { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:visited { \n" +
"    color: #2ba6cb !important;\n" +
"  }\n" +
"\n" +
"  h1 a, \n" +
"  h2 a, \n" +
"  h3 a, \n" +
"  h4 a, \n" +
"  h5 a, \n" +
"  h6 a {\n" +
"    color: #2ba6cb;\n" +
"  }\n" +
"\n" +
"  h1 a:active, \n" +
"  h2 a:active,  \n" +
"  h3 a:active, \n" +
"  h4 a:active, \n" +
"  h5 a:active, \n" +
"  h6 a:active { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  h1 a:visited, \n" +
"  h2 a:visited,  \n" +
"  h3 a:visited, \n" +
"  h4 a:visited, \n" +
"  h5 a:visited, \n" +
"  h6 a:visited { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  /* Panels */\n" +
"\n" +
"  .panel {\n" +
"    background: #f2f2f2;\n" +
"    border: 1px solid #d9d9d9;\n" +
"    padding: 10px !important;\n" +
"  }\n" +
"\n" +
"  .sub-grid table {\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  .sub-grid td.sub-columns {\n" +
"    padding-bottom: 0;\n" +
"  }\n" +
"\n" +
"  /* Buttons */\n" +
"\n" +
"  table.button,\n" +
"  table.tiny-button,\n" +
"  table.small-button,\n" +
"  table.medium-button,\n" +
"  table.large-button {\n" +
"    width: 100%;\n" +
"    overflow: hidden;\n" +
"  }\n" +
"\n" +
"  table.button td,\n" +
"  table.tiny-button td,\n" +
"  table.small-button td,\n" +
"  table.medium-button td,\n" +
"  table.large-button td {\n" +
"    display: block;\n" +
"    width: auto !important;\n" +
"    text-align: center;\n" +
"    background: #2ba6cb;\n" +
"    border: 1px solid #2284a1;\n" +
"    color: #ffffff;\n" +
"    padding: 8px 0;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td {\n" +
"    padding: 5px 0 4px;\n" +
"  }\n" +
"\n" +
"  table.small-button td {\n" +
"    padding: 8px 0 7px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td {\n" +
"    padding: 12px 0 10px;\n" +
"  }\n" +
"\n" +
"  table.large-button td {\n" +
"    padding: 21px 0 18px;\n" +
"  }\n" +
"\n" +
"  table.button td a,\n" +
"  table.tiny-button td a,\n" +
"  table.small-button td a,\n" +
"  table.medium-button td a,\n" +
"  table.large-button td a {\n" +
"    font-weight: bold;\n" +
"    text-decoration: none;\n" +
"    font-family: Helvetica, Arial, sans-serif;\n" +
"    color: #ffffff;\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td a {\n" +
"    font-size: 12px;\n" +
"    font-weight: normal;\n" +
"  }\n" +
"\n" +
"  table.small-button td a {\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td a {\n" +
"    font-size: 20px;\n" +
"  }\n" +
"\n" +
"  table.large-button td a {\n" +
"    font-size: 24px;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.button:visited td,\n" +
"  table.button:active td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:visited td a,\n" +
"  table.button:active td a {\n" +
"    color: #fff !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.tiny-button:hover td,\n" +
"  table.small-button:hover td,\n" +
"  table.medium-button:hover td,\n" +
"  table.large-button:hover td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:active td a,\n" +
"  table.button td a:visited,\n" +
"  table.tiny-button:hover td a,\n" +
"  table.tiny-button:active td a,\n" +
"  table.tiny-button td a:visited,\n" +
"  table.small-button:hover td a,\n" +
"  table.small-button:active td a,\n" +
"  table.small-button td a:visited,\n" +
"  table.medium-button:hover td a,\n" +
"  table.medium-button:active td a,\n" +
"  table.medium-button td a:visited,\n" +
"  table.large-button:hover td a,\n" +
"  table.large-button:active td a,\n" +
"  table.large-button td a:visited {\n" +
"    color: #ffffff !important; \n" +
"  }\n" +
"\n" +
"  table.secondary td {\n" +
"    background: #e9e9e9;\n" +
"    border-color: #d0d0d0;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary td a {\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td {\n" +
"    background: #d0d0d0 !important;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td a,\n" +
"  table.secondary td a:visited,\n" +
"  table.secondary:active td a {\n" +
"    color: #555 !important;\n" +
"  }\n" +
"\n" +
"  table.success td {\n" +
"    background: #5da423;\n" +
"    border-color: #457a1a;\n" +
"  }\n" +
"\n" +
"  table.success:hover td {\n" +
"    background: #457a1a !important;\n" +
"  }\n" +
"\n" +
"  table.alert td {\n" +
"    background: #c60f13;\n" +
"    border-color: #970b0e;\n" +
"  }\n" +
"\n" +
"  table.alert:hover td {\n" +
"    background: #970b0e !important;\n" +
"  }\n" +
"\n" +
"  table.radius td {\n" +
"    -webkit-border-radius: 3px;\n" +
"    -moz-border-radius: 3px;\n" +
"    border-radius: 3px;\n" +
"  }\n" +
"\n" +
"  table.round td {\n" +
"    -webkit-border-radius: 500px;\n" +
"    -moz-border-radius: 500px;\n" +
"    border-radius: 500px;\n" +
"  }\n" +
"\n" +
"  /* Outlook First */\n" +
"\n" +
"  body.outlook p {\n" +
"    display: inline !important;\n" +
"  }\n" +
"\n" +
"  /*  Media Queries */\n" +
"\n" +
"  @media only screen and (max-width: 600px) {\n" +
"\n" +
"    table[class=\"body\"] img {\n" +
"      width: auto !important;\n" +
"      height: auto !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] center {\n" +
"      min-width: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .container {\n" +
"      width: 95% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .row {\n" +
"      width: 100% !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper {\n" +
"      display: block !important;\n" +
"      padding-right: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns,\n" +
"    table[class=\"body\"] .column {\n" +
"      table-layout: fixed !important;\n" +
"      float: none !important;\n" +
"      width: 100% !important;\n" +
"      padding-right: 0px !important;\n" +
"      padding-left: 0px !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper.first .columns,\n" +
"    table[class=\"body\"] .wrapper.first .column {\n" +
"      display: table !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td,\n" +
"    table[class=\"body\"] table.column td {\n" +
"      width: 100% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns td.one,\n" +
"    table[class=\"body\"] .column td.one { width: 8.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.two,\n" +
"    table[class=\"body\"] .column td.two { width: 16.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.three,\n" +
"    table[class=\"body\"] .column td.three { width: 25% !important; }\n" +
"    table[class=\"body\"] .columns td.four,\n" +
"    table[class=\"body\"] .column td.four { width: 33.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.five,\n" +
"    table[class=\"body\"] .column td.five { width: 41.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.six,\n" +
"    table[class=\"body\"] .column td.six { width: 50% !important; }\n" +
"    table[class=\"body\"] .columns td.seven,\n" +
"    table[class=\"body\"] .column td.seven { width: 58.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eight,\n" +
"    table[class=\"body\"] .column td.eight { width: 66.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.nine,\n" +
"    table[class=\"body\"] .column td.nine { width: 75% !important; }\n" +
"    table[class=\"body\"] .columns td.ten,\n" +
"    table[class=\"body\"] .column td.ten { width: 83.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eleven,\n" +
"    table[class=\"body\"] .column td.eleven { width: 91.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.twelve,\n" +
"    table[class=\"body\"] .column td.twelve { width: 100% !important; }\n" +
"\n" +
"    table[class=\"body\"] td.offset-by-one,\n" +
"    table[class=\"body\"] td.offset-by-two,\n" +
"    table[class=\"body\"] td.offset-by-three,\n" +
"    table[class=\"body\"] td.offset-by-four,\n" +
"    table[class=\"body\"] td.offset-by-five,\n" +
"    table[class=\"body\"] td.offset-by-six,\n" +
"    table[class=\"body\"] td.offset-by-seven,\n" +
"    table[class=\"body\"] td.offset-by-eight,\n" +
"    table[class=\"body\"] td.offset-by-nine,\n" +
"    table[class=\"body\"] td.offset-by-ten,\n" +
"    table[class=\"body\"] td.offset-by-eleven {\n" +
"      padding-left: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td.expander {\n" +
"      width: 1px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .right-text-pad,\n" +
"    table[class=\"body\"] .text-pad-right {\n" +
"      padding-left: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .left-text-pad,\n" +
"    table[class=\"body\"] .text-pad-left {\n" +
"      padding-right: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .hide-for-small,\n" +
"    table[class=\"body\"] .show-for-desktop {\n" +
"      display: none !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .show-for-small,\n" +
"    table[class=\"body\"] .hide-for-desktop {\n" +
"      display: inherit !important;\n" +
"    }\n" +
"  }\n" +
"\n" +
"    </style>\n" +
"    <style>\n" +
"\n" +
"      table.facebook td {\n" +
"        background: #3b5998;\n" +
"        border-color: #2d4473;\n" +
"      }\n" +
"\n" +
"      table.facebook:hover td {\n" +
"        background: #2d4473 !important;\n" +
"      }\n" +
"\n" +
"      table.twitter td {\n" +
"        background: #00acee;\n" +
"        border-color: #0087bb;\n" +
"      }\n" +
"\n" +
"      table.twitter:hover td {\n" +
"        background: #0087bb !important;\n" +
"      }\n" +
"\n" +
"      table.google-plus td {\n" +
"        background-color: #DB4A39;\n" +
"        border-color: #CC0000;\n" +
"      }\n" +
"\n" +
"      table.google-plus:hover td {\n" +
"        background: #CC0000 !important;\n" +
"      }\n" +
"\n" +
"      .template-label {\n" +
"        color: #ffffff;\n" +
"        font-weight: bold;\n" +
"        font-size: 11px;\n" +
"      }\n" +
"\n" +
"      .callout .panel {\n" +
"        background: #ECF8FF;\n" +
"        border-color: #b9e5ff;\n" +
"      }\n" +
"\n" +
"      .header {\n" +
"        background: #999999;\n" +
"      }\n" +
"\n" +
"      .footer .wrapper {\n" +
"        background: #ebebeb;\n" +
"      }\n" +
"\n" +
"      .footer h5 {\n" +
"        padding-bottom: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .text-pad {\n" +
"        padding-left: 10px;\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .left-text-pad {\n" +
"        padding-left: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .right-text-pad {\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      @media only screen and (max-width: 600px) {\n" +
"\n" +
"        table[class=\"body\"] .right-text-pad {\n" +
"          padding-left: 10px !important;\n" +
"        }\n" +
"\n" +
"        table[class=\"body\"] .left-text-pad {\n" +
"          padding-right: 10px !important;\n" +
"        }\n" +
"      }\n" +
"  </style>\n" +
"  <style>\n" +
"\n" +
"    table.facebook td {\n" +
"      background: #3b5998;\n" +
"      border-color: #2d4473;\n" +
"    }\n" +
"\n" +
"    table.facebook:hover td {\n" +
"      background: #2d4473 !important;\n" +
"    }\n" +
"\n" +
"    table.twitter td {\n" +
"      background: #00acee;\n" +
"      border-color: #0087bb;\n" +
"    }\n" +
"\n" +
"    table.twitter:hover td {\n" +
"      background: #0087bb !important;\n" +
"    }\n" +
"\n" +
"    table.google-plus td {\n" +
"      background-color: #DB4A39;\n" +
"      border-color: #CC0000;\n" +
"    }\n" +
"\n" +
"    table.google-plus:hover td {\n" +
"      background: #CC0000 !important;\n" +
"    }\n" +
"\n" +
"    .template-label {\n" +
"      color: #ffffff;\n" +
"      font-weight: bold;\n" +
"      font-size: 11px;\n" +
"    }\n" +
"\n" +
"    .callout .wrapper {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"\n" +
"    .callout .panel {\n" +
"      background: #ECF8FF;\n" +
"      border-color: #b9e5ff;\n" +
"    }\n" +
"\n" +
"    .header {\n" +
"      background: #999999;\n" +
"    }\n" +
"\n" +
"    .footer .wrapper {\n" +
"      background: #ebebeb;\n" +
"    }\n" +
"\n" +
"    .footer h5 {\n" +
"      padding-bottom: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .text-pad {\n" +
"      padding-left: 10px;\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .left-text-pad {\n" +
"      padding-left: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .right-text-pad {\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    @media only screen and (max-width: 600px) {\n" +
"\n" +
"      table[class=\"body\"] .right-text-pad {\n" +
"        padding-left: 10px !important;\n" +
"      }\n" +
"\n" +
"      table[class=\"body\"] .left-text-pad {\n" +
"        padding-right: 10px !important;\n" +
"      }\n" +
"    }\n" +
"\n" +
"	</style>\n" +
"</head>\n" +
"<body>\n" +
"	<table class=\"body\">\n" +
"		<tr>\n" +
"			<td class=\"center\" align=\"center\" valign=\"top\">\n" +
"        <center>\n" +
"\n" +
"          <table class=\"row header\">\n" +
"            <tr>\n" +
"              <td class=\"center\" align=\"center\">\n" +
"                <center>\n" +
"\n" +
"                  <table class=\"container\">\n" +
"                    <tr>\n" +
"                      <td class=\"wrapper last\">\n" +
"\n" +
"                        <table class=\"twelve columns\">\n" +
"                          <tr>\n" +
"                            <td class=\"six sub-columns\">\n" +
"                              <h1 style=\"color: #009EC3; text-shadow:1px 1px black;\">PlayChat</h1>\n" +
"                            </td>\n" +
"                            <td class=\"six sub-columns last\" style=\"text-align:right; vertical-align:middle;\">\n" +
"                              <span class=\"template-label\" style=\"font-size: 15px;\">Join room</span>\n" +
"                            </td>\n" +
"                            <td class=\"expander\"></td>\n" +
"                          </tr>\n" +
"                        </table>\n" +
"\n" +
"                      </td>\n" +
"                    </tr>\n" +
"                  </table>\n" +
"\n" +
"                </center>\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"          <table class=\"container\">\n" +
"            <tr>\n" +
"              <td>\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td>\n" +
"                            <h1>Hi, "+username+"</h1>\n" +
"                						<p class=\"lead\">User "+invitator + " has invited you to join his chatroom "+roomName+" </p>\n" +
"                						<p>To confirm invitation please Follow the link below:</p>\n" +
"                            <p>Invitation link: <a href='"+url+"'>"+url+"</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row callout\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row footer\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"left-text-pad\">\n" +
"\n" +
"                            <h5>Connect With Us:</h5>\n" +
"\n" +
"                            <table class=\"tiny-button facebook\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Facebook</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button twitter\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Twitter</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button google-plus\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Google +</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"last right-text-pad\">\n" +
"                            <h5>Contact Info:</h5>\n" +
"                            <p>Phone: 408.341.0600</p>\n" +
"                            <p>Email: <a href=\"mailto:hseldon@trantor.com\">example@playchat.com</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td align=\"center\">\n" +
"                            <center>\n" +
"                              <p style=\"text-align:center;\"><a href=\"#\">Terms</a> | <a href=\"#\">Privacy</a> | <a href=\"#\">Unsubscribe</a></p>\n" +
"                            </center>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"              <!-- container end below -->\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"        </center>\n" +
"			</td>\n" +
"		</tr>\n" +
"	</table>\n" +
"</body>\n" +
"</html>";
    }  
    public static String sendInviteSuccess (String username,String roomName){
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"<head>\n" +
"	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"	<meta name=\"viewport\" content=\"width=device-width\"/>\n" +
"	<style>\n" +
"    \n" +
"  #outlook a { \n" +
"    padding:0; \n" +
"  } \n" +
"\n" +
"  body{ \n" +
"    width:100% !important; \n" +
"    min-width: 100%;\n" +
"    -webkit-text-size-adjust:100%; \n" +
"    -ms-text-size-adjust:100%; \n" +
"    margin:0; \n" +
"    padding:0;\n" +
"  }\n" +
"\n" +
"  .ExternalClass { \n" +
"    width:100%;\n" +
"  } \n" +
"\n" +
"  .ExternalClass, \n" +
"  .ExternalClass p, \n" +
"  .ExternalClass span, \n" +
"  .ExternalClass font, \n" +
"  .ExternalClass td, \n" +
"  .ExternalClass div { \n" +
"    line-height: 100%; \n" +
"  } \n" +
"\n" +
"  #backgroundTable { \n" +
"    margin:0; \n" +
"    padding:0; \n" +
"    width:100% !important; \n" +
"    line-height: 100% !important; \n" +
"  }\n" +
"\n" +
"  img { \n" +
"    outline:none; \n" +
"    text-decoration:none; \n" +
"    -ms-interpolation-mode: bicubic;\n" +
"    width: auto;\n" +
"    max-width: 100%; \n" +
"    float: left; \n" +
"    clear: both; \n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  center {\n" +
"    width: 100%;\n" +
"    min-width: 580px;\n" +
"  }\n" +
"\n" +
"  a img { \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  p {\n" +
"    margin: 0 0 0 10px;\n" +
"  }\n" +
"\n" +
"  table {\n" +
"    border-spacing: 0;\n" +
"    border-collapse: collapse;\n" +
"  }\n" +
"\n" +
"  td { \n" +
"    word-break: break-word;\n" +
"    -webkit-hyphens: auto;\n" +
"    -moz-hyphens: auto;\n" +
"    hyphens: auto;\n" +
"    border-collapse: collapse !important; \n" +
"  }\n" +
"\n" +
"  table, tr, td {\n" +
"    padding: 0;\n" +
"    vertical-align: top;\n" +
"    text-align: left;\n" +
"  }\n" +
"\n" +
"  hr {\n" +
"    color: #d9d9d9; \n" +
"    background-color: #d9d9d9; \n" +
"    height: 1px; \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  /* Responsive Grid */\n" +
"\n" +
"  table.body {\n" +
"    height: 100%;\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  table.container {\n" +
"    width: 580px;\n" +
"    margin: 0 auto;\n" +
"    text-align: inherit;\n" +
"  }\n" +
"\n" +
"  table.row { \n" +
"    padding: 0px; \n" +
"    width: 100%;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.container table.row {\n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  td.wrapper {\n" +
"    padding: 10px 20px 0px 0px;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.columns,\n" +
"  table.column {\n" +
"    margin: 0 auto;\n" +
"  }\n" +
"\n" +
"  table.columns td,\n" +
"  table.column td {\n" +
"    padding: 0px 0px 10px; \n" +
"  }\n" +
"\n" +
"  table.columns td.sub-columns,\n" +
"  table.column td.sub-columns,\n" +
"  table.columns td.sub-column,\n" +
"  table.column td.sub-column {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  td.sub-column, td.sub-columns {\n" +
"    min-width: 0px;\n" +
"  }\n" +
"\n" +
"  table.row td.last,\n" +
"  table.container td.last {\n" +
"    padding-right: 0px;\n" +
"  }\n" +
"\n" +
"  table.one { width: 30px; }\n" +
"  table.two { width: 80px; }\n" +
"  table.three { width: 130px; }\n" +
"  table.four { width: 180px; }\n" +
"  table.five { width: 230px; }\n" +
"  table.six { width: 280px; }\n" +
"  table.seven { width: 330px; }\n" +
"  table.eight { width: 380px; }\n" +
"  table.nine { width: 430px; }\n" +
"  table.ten { width: 480px; }\n" +
"  table.eleven { width: 530px; }\n" +
"  table.twelve { width: 580px; }\n" +
"\n" +
"  table.one center { min-width: 30px; }\n" +
"  table.two center { min-width: 80px; }\n" +
"  table.three center { min-width: 130px; }\n" +
"  table.four center { min-width: 180px; }\n" +
"  table.five center { min-width: 230px; }\n" +
"  table.six center { min-width: 280px; }\n" +
"  table.seven center { min-width: 330px; }\n" +
"  table.eight center { min-width: 380px; }\n" +
"  table.nine center { min-width: 430px; }\n" +
"  table.ten center { min-width: 480px; }\n" +
"  table.eleven center { min-width: 530px; }\n" +
"  table.twelve center { min-width: 580px; }\n" +
"\n" +
"  table.one .panel center { min-width: 10px; }\n" +
"  table.two .panel center { min-width: 60px; }\n" +
"  table.three .panel center { min-width: 110px; }\n" +
"  table.four .panel center { min-width: 160px; }\n" +
"  table.five .panel center { min-width: 210px; }\n" +
"  table.six .panel center { min-width: 260px; }\n" +
"  table.seven .panel center { min-width: 310px; }\n" +
"  table.eight .panel center { min-width: 360px; }\n" +
"  table.nine .panel center { min-width: 410px; }\n" +
"  table.ten .panel center { min-width: 460px; }\n" +
"  table.eleven .panel center { min-width: 510px; }\n" +
"  table.twelve .panel center { min-width: 560px; }\n" +
"\n" +
"  .body .columns td.one,\n" +
"  .body .column td.one { width: 8.333333%; }\n" +
"  .body .columns td.two,\n" +
"  .body .column td.two { width: 16.666666%; }\n" +
"  .body .columns td.three,\n" +
"  .body .column td.three { width: 25%; }\n" +
"  .body .columns td.four,\n" +
"  .body .column td.four { width: 33.333333%; }\n" +
"  .body .columns td.five,\n" +
"  .body .column td.five { width: 41.666666%; }\n" +
"  .body .columns td.six,\n" +
"  .body .column td.six { width: 50%; }\n" +
"  .body .columns td.seven,\n" +
"  .body .column td.seven { width: 58.333333%; }\n" +
"  .body .columns td.eight,\n" +
"  .body .column td.eight { width: 66.666666%; }\n" +
"  .body .columns td.nine,\n" +
"  .body .column td.nine { width: 75%; }\n" +
"  .body .columns td.ten,\n" +
"  .body .column td.ten { width: 83.333333%; }\n" +
"  .body .columns td.eleven,\n" +
"  .body .column td.eleven { width: 91.666666%; }\n" +
"  .body .columns td.twelve,\n" +
"  .body .column td.twelve { width: 100%; }\n" +
"\n" +
"  td.offset-by-one { padding-left: 50px; }\n" +
"  td.offset-by-two { padding-left: 100px; }\n" +
"  td.offset-by-three { padding-left: 150px; }\n" +
"  td.offset-by-four { padding-left: 200px; }\n" +
"  td.offset-by-five { padding-left: 250px; }\n" +
"  td.offset-by-six { padding-left: 300px; }\n" +
"  td.offset-by-seven { padding-left: 350px; }\n" +
"  td.offset-by-eight { padding-left: 400px; }\n" +
"  td.offset-by-nine { padding-left: 450px; }\n" +
"  td.offset-by-ten { padding-left: 500px; }\n" +
"  td.offset-by-eleven { padding-left: 550px; }\n" +
"\n" +
"  td.expander {\n" +
"    visibility: hidden;\n" +
"    width: 0px;\n" +
"    padding: 0 !important;\n" +
"  }\n" +
"\n" +
"  table.columns .text-pad,\n" +
"  table.column .text-pad {\n" +
"    padding-left: 10px;\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .left-text-pad,\n" +
"  table.columns .text-pad-left,\n" +
"  table.column .left-text-pad,\n" +
"  table.column .text-pad-left {\n" +
"    padding-left: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .right-text-pad,\n" +
"  table.columns .text-pad-right,\n" +
"  table.column .right-text-pad,\n" +
"  table.column .text-pad-right {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  /* Block Grid */\n" +
"\n" +
"  .block-grid {\n" +
"    width: 100%;\n" +
"    max-width: 580px;\n" +
"  }\n" +
"\n" +
"  .block-grid td {\n" +
"    display: inline-block;\n" +
"    padding:10px;\n" +
"  }\n" +
"\n" +
"  .two-up td {\n" +
"    width:270px;\n" +
"  }\n" +
"\n" +
"  .three-up td {\n" +
"    width:173px;\n" +
"  }\n" +
"\n" +
"  .four-up td {\n" +
"    width:125px;\n" +
"  }\n" +
"\n" +
"  .five-up td {\n" +
"    width:96px;\n" +
"  }\n" +
"\n" +
"  .six-up td {\n" +
"    width:76px;\n" +
"  }\n" +
"\n" +
"  .seven-up td {\n" +
"    width:62px;\n" +
"  }\n" +
"\n" +
"  .eight-up td {\n" +
"    width:52px;\n" +
"  }\n" +
"\n" +
"  /* Alignment & Visibility Classes */\n" +
"\n" +
"  table.center, td.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  h1.center,\n" +
"  h2.center,\n" +
"  h3.center,\n" +
"  h4.center,\n" +
"  h5.center,\n" +
"  h6.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  span.center {\n" +
"    display: block;\n" +
"    width: 100%;\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  img.center {\n" +
"    margin: 0 auto;\n" +
"    float: none;\n" +
"  }\n" +
"\n" +
"  .show-for-small,\n" +
"  .hide-for-desktop {\n" +
"    display: none;\n" +
"  }\n" +
"\n" +
"  /* Typography */\n" +
"\n" +
"  body, table.body, h1, h2, h3, h4, h5, h6, p, td { \n" +
"    color: #222222;\n" +
"    font-family: \"Helvetica\", \"Arial\", sans-serif; \n" +
"    font-weight: normal; \n" +
"    padding:0; \n" +
"    margin: 0;\n" +
"    text-align: left; \n" +
"    line-height: 1.3;\n" +
"  }\n" +
"\n" +
"  h1, h2, h3, h4, h5, h6 {\n" +
"    word-break: normal;\n" +
"  }\n" +
"\n" +
"  h1 {font-size: 40px;}\n" +
"  h2 {font-size: 36px;}\n" +
"  h3 {font-size: 32px;}\n" +
"  h4 {font-size: 28px;}\n" +
"  h5 {font-size: 24px;}\n" +
"  h6 {font-size: 20px;}\n" +
"  body, table.body, p, td {font-size: 14px;line-height:19px;}\n" +
"\n" +
"  p.lead, p.lede, p.leed {\n" +
"    font-size: 18px;\n" +
"    line-height:21px;\n" +
"  }\n" +
"\n" +
"  p { \n" +
"    margin-bottom: 10px;\n" +
"  }\n" +
"\n" +
"  small {\n" +
"    font-size: 10px;\n" +
"  }\n" +
"\n" +
"  a {\n" +
"    color: #2ba6cb; \n" +
"    text-decoration: none;\n" +
"  }\n" +
"\n" +
"  a:hover { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:active { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:visited { \n" +
"    color: #2ba6cb !important;\n" +
"  }\n" +
"\n" +
"  h1 a, \n" +
"  h2 a, \n" +
"  h3 a, \n" +
"  h4 a, \n" +
"  h5 a, \n" +
"  h6 a {\n" +
"    color: #2ba6cb;\n" +
"  }\n" +
"\n" +
"  h1 a:active, \n" +
"  h2 a:active,  \n" +
"  h3 a:active, \n" +
"  h4 a:active, \n" +
"  h5 a:active, \n" +
"  h6 a:active { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  h1 a:visited, \n" +
"  h2 a:visited,  \n" +
"  h3 a:visited, \n" +
"  h4 a:visited, \n" +
"  h5 a:visited, \n" +
"  h6 a:visited { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  /* Panels */\n" +
"\n" +
"  .panel {\n" +
"    background: #f2f2f2;\n" +
"    border: 1px solid #d9d9d9;\n" +
"    padding: 10px !important;\n" +
"  }\n" +
"\n" +
"  .sub-grid table {\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  .sub-grid td.sub-columns {\n" +
"    padding-bottom: 0;\n" +
"  }\n" +
"\n" +
"  /* Buttons */\n" +
"\n" +
"  table.button,\n" +
"  table.tiny-button,\n" +
"  table.small-button,\n" +
"  table.medium-button,\n" +
"  table.large-button {\n" +
"    width: 100%;\n" +
"    overflow: hidden;\n" +
"  }\n" +
"\n" +
"  table.button td,\n" +
"  table.tiny-button td,\n" +
"  table.small-button td,\n" +
"  table.medium-button td,\n" +
"  table.large-button td {\n" +
"    display: block;\n" +
"    width: auto !important;\n" +
"    text-align: center;\n" +
"    background: #2ba6cb;\n" +
"    border: 1px solid #2284a1;\n" +
"    color: #ffffff;\n" +
"    padding: 8px 0;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td {\n" +
"    padding: 5px 0 4px;\n" +
"  }\n" +
"\n" +
"  table.small-button td {\n" +
"    padding: 8px 0 7px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td {\n" +
"    padding: 12px 0 10px;\n" +
"  }\n" +
"\n" +
"  table.large-button td {\n" +
"    padding: 21px 0 18px;\n" +
"  }\n" +
"\n" +
"  table.button td a,\n" +
"  table.tiny-button td a,\n" +
"  table.small-button td a,\n" +
"  table.medium-button td a,\n" +
"  table.large-button td a {\n" +
"    font-weight: bold;\n" +
"    text-decoration: none;\n" +
"    font-family: Helvetica, Arial, sans-serif;\n" +
"    color: #ffffff;\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td a {\n" +
"    font-size: 12px;\n" +
"    font-weight: normal;\n" +
"  }\n" +
"\n" +
"  table.small-button td a {\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td a {\n" +
"    font-size: 20px;\n" +
"  }\n" +
"\n" +
"  table.large-button td a {\n" +
"    font-size: 24px;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.button:visited td,\n" +
"  table.button:active td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:visited td a,\n" +
"  table.button:active td a {\n" +
"    color: #fff !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.tiny-button:hover td,\n" +
"  table.small-button:hover td,\n" +
"  table.medium-button:hover td,\n" +
"  table.large-button:hover td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:active td a,\n" +
"  table.button td a:visited,\n" +
"  table.tiny-button:hover td a,\n" +
"  table.tiny-button:active td a,\n" +
"  table.tiny-button td a:visited,\n" +
"  table.small-button:hover td a,\n" +
"  table.small-button:active td a,\n" +
"  table.small-button td a:visited,\n" +
"  table.medium-button:hover td a,\n" +
"  table.medium-button:active td a,\n" +
"  table.medium-button td a:visited,\n" +
"  table.large-button:hover td a,\n" +
"  table.large-button:active td a,\n" +
"  table.large-button td a:visited {\n" +
"    color: #ffffff !important; \n" +
"  }\n" +
"\n" +
"  table.secondary td {\n" +
"    background: #e9e9e9;\n" +
"    border-color: #d0d0d0;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary td a {\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td {\n" +
"    background: #d0d0d0 !important;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td a,\n" +
"  table.secondary td a:visited,\n" +
"  table.secondary:active td a {\n" +
"    color: #555 !important;\n" +
"  }\n" +
"\n" +
"  table.success td {\n" +
"    background: #5da423;\n" +
"    border-color: #457a1a;\n" +
"  }\n" +
"\n" +
"  table.success:hover td {\n" +
"    background: #457a1a !important;\n" +
"  }\n" +
"\n" +
"  table.alert td {\n" +
"    background: #c60f13;\n" +
"    border-color: #970b0e;\n" +
"  }\n" +
"\n" +
"  table.alert:hover td {\n" +
"    background: #970b0e !important;\n" +
"  }\n" +
"\n" +
"  table.radius td {\n" +
"    -webkit-border-radius: 3px;\n" +
"    -moz-border-radius: 3px;\n" +
"    border-radius: 3px;\n" +
"  }\n" +
"\n" +
"  table.round td {\n" +
"    -webkit-border-radius: 500px;\n" +
"    -moz-border-radius: 500px;\n" +
"    border-radius: 500px;\n" +
"  }\n" +
"\n" +
"  /* Outlook First */\n" +
"\n" +
"  body.outlook p {\n" +
"    display: inline !important;\n" +
"  }\n" +
"\n" +
"  /*  Media Queries */\n" +
"\n" +
"  @media only screen and (max-width: 600px) {\n" +
"\n" +
"    table[class=\"body\"] img {\n" +
"      width: auto !important;\n" +
"      height: auto !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] center {\n" +
"      min-width: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .container {\n" +
"      width: 95% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .row {\n" +
"      width: 100% !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper {\n" +
"      display: block !important;\n" +
"      padding-right: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns,\n" +
"    table[class=\"body\"] .column {\n" +
"      table-layout: fixed !important;\n" +
"      float: none !important;\n" +
"      width: 100% !important;\n" +
"      padding-right: 0px !important;\n" +
"      padding-left: 0px !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper.first .columns,\n" +
"    table[class=\"body\"] .wrapper.first .column {\n" +
"      display: table !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td,\n" +
"    table[class=\"body\"] table.column td {\n" +
"      width: 100% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns td.one,\n" +
"    table[class=\"body\"] .column td.one { width: 8.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.two,\n" +
"    table[class=\"body\"] .column td.two { width: 16.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.three,\n" +
"    table[class=\"body\"] .column td.three { width: 25% !important; }\n" +
"    table[class=\"body\"] .columns td.four,\n" +
"    table[class=\"body\"] .column td.four { width: 33.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.five,\n" +
"    table[class=\"body\"] .column td.five { width: 41.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.six,\n" +
"    table[class=\"body\"] .column td.six { width: 50% !important; }\n" +
"    table[class=\"body\"] .columns td.seven,\n" +
"    table[class=\"body\"] .column td.seven { width: 58.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eight,\n" +
"    table[class=\"body\"] .column td.eight { width: 66.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.nine,\n" +
"    table[class=\"body\"] .column td.nine { width: 75% !important; }\n" +
"    table[class=\"body\"] .columns td.ten,\n" +
"    table[class=\"body\"] .column td.ten { width: 83.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eleven,\n" +
"    table[class=\"body\"] .column td.eleven { width: 91.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.twelve,\n" +
"    table[class=\"body\"] .column td.twelve { width: 100% !important; }\n" +
"\n" +
"    table[class=\"body\"] td.offset-by-one,\n" +
"    table[class=\"body\"] td.offset-by-two,\n" +
"    table[class=\"body\"] td.offset-by-three,\n" +
"    table[class=\"body\"] td.offset-by-four,\n" +
"    table[class=\"body\"] td.offset-by-five,\n" +
"    table[class=\"body\"] td.offset-by-six,\n" +
"    table[class=\"body\"] td.offset-by-seven,\n" +
"    table[class=\"body\"] td.offset-by-eight,\n" +
"    table[class=\"body\"] td.offset-by-nine,\n" +
"    table[class=\"body\"] td.offset-by-ten,\n" +
"    table[class=\"body\"] td.offset-by-eleven {\n" +
"      padding-left: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td.expander {\n" +
"      width: 1px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .right-text-pad,\n" +
"    table[class=\"body\"] .text-pad-right {\n" +
"      padding-left: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .left-text-pad,\n" +
"    table[class=\"body\"] .text-pad-left {\n" +
"      padding-right: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .hide-for-small,\n" +
"    table[class=\"body\"] .show-for-desktop {\n" +
"      display: none !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .show-for-small,\n" +
"    table[class=\"body\"] .hide-for-desktop {\n" +
"      display: inherit !important;\n" +
"    }\n" +
"  }\n" +
"\n" +
"    </style>\n" +
"    <style>\n" +
"\n" +
"      table.facebook td {\n" +
"        background: #3b5998;\n" +
"        border-color: #2d4473;\n" +
"      }\n" +
"\n" +
"      table.facebook:hover td {\n" +
"        background: #2d4473 !important;\n" +
"      }\n" +
"\n" +
"      table.twitter td {\n" +
"        background: #00acee;\n" +
"        border-color: #0087bb;\n" +
"      }\n" +
"\n" +
"      table.twitter:hover td {\n" +
"        background: #0087bb !important;\n" +
"      }\n" +
"\n" +
"      table.google-plus td {\n" +
"        background-color: #DB4A39;\n" +
"        border-color: #CC0000;\n" +
"      }\n" +
"\n" +
"      table.google-plus:hover td {\n" +
"        background: #CC0000 !important;\n" +
"      }\n" +
"\n" +
"      .template-label {\n" +
"        color: #ffffff;\n" +
"        font-weight: bold;\n" +
"        font-size: 11px;\n" +
"      }\n" +
"\n" +
"      .callout .panel {\n" +
"        background: #ECF8FF;\n" +
"        border-color: #b9e5ff;\n" +
"      }\n" +
"\n" +
"      .header {\n" +
"        background: #999999;\n" +
"      }\n" +
"\n" +
"      .footer .wrapper {\n" +
"        background: #ebebeb;\n" +
"      }\n" +
"\n" +
"      .footer h5 {\n" +
"        padding-bottom: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .text-pad {\n" +
"        padding-left: 10px;\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .left-text-pad {\n" +
"        padding-left: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .right-text-pad {\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      @media only screen and (max-width: 600px) {\n" +
"\n" +
"        table[class=\"body\"] .right-text-pad {\n" +
"          padding-left: 10px !important;\n" +
"        }\n" +
"\n" +
"        table[class=\"body\"] .left-text-pad {\n" +
"          padding-right: 10px !important;\n" +
"        }\n" +
"      }\n" +
"  </style>\n" +
"  <style>\n" +
"\n" +
"    table.facebook td {\n" +
"      background: #3b5998;\n" +
"      border-color: #2d4473;\n" +
"    }\n" +
"\n" +
"    table.facebook:hover td {\n" +
"      background: #2d4473 !important;\n" +
"    }\n" +
"\n" +
"    table.twitter td {\n" +
"      background: #00acee;\n" +
"      border-color: #0087bb;\n" +
"    }\n" +
"\n" +
"    table.twitter:hover td {\n" +
"      background: #0087bb !important;\n" +
"    }\n" +
"\n" +
"    table.google-plus td {\n" +
"      background-color: #DB4A39;\n" +
"      border-color: #CC0000;\n" +
"    }\n" +
"\n" +
"    table.google-plus:hover td {\n" +
"      background: #CC0000 !important;\n" +
"    }\n" +
"\n" +
"    .template-label {\n" +
"      color: #ffffff;\n" +
"      font-weight: bold;\n" +
"      font-size: 11px;\n" +
"    }\n" +
"\n" +
"    .callout .wrapper {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"\n" +
"    .callout .panel {\n" +
"      background: #ECF8FF;\n" +
"      border-color: #b9e5ff;\n" +
"    }\n" +
"\n" +
"    .header {\n" +
"      background: #999999;\n" +
"    }\n" +
"\n" +
"    .footer .wrapper {\n" +
"      background: #ebebeb;\n" +
"    }\n" +
"\n" +
"    .footer h5 {\n" +
"      padding-bottom: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .text-pad {\n" +
"      padding-left: 10px;\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .left-text-pad {\n" +
"      padding-left: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .right-text-pad {\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    @media only screen and (max-width: 600px) {\n" +
"\n" +
"      table[class=\"body\"] .right-text-pad {\n" +
"        padding-left: 10px !important;\n" +
"      }\n" +
"\n" +
"      table[class=\"body\"] .left-text-pad {\n" +
"        padding-right: 10px !important;\n" +
"      }\n" +
"    }\n" +
"\n" +
"	</style>\n" +
"</head>\n" +
"<body>\n" +
"	<table class=\"body\">\n" +
"		<tr>\n" +
"			<td class=\"center\" align=\"center\" valign=\"top\">\n" +
"        <center>\n" +
"\n" +
"          <table class=\"row header\">\n" +
"            <tr>\n" +
"              <td class=\"center\" align=\"center\">\n" +
"                <center>\n" +
"\n" +
"                  <table class=\"container\">\n" +
"                    <tr>\n" +
"                      <td class=\"wrapper last\">\n" +
"\n" +
"                        <table class=\"twelve columns\">\n" +
"                          <tr>\n" +
"                            <td class=\"six sub-columns\">\n" +
"                              <h1 style=\"color: #009EC3; text-shadow:1px 1px black;\">PlayChat</h1>\n" +
"                            </td>\n" +
"                            <td class=\"six sub-columns last\" style=\"text-align:right; vertical-align:middle;\">\n" +
"                              <span class=\"template-label\" style=\"font-size: 15px;\">Join room</span>\n" +
"                            </td>\n" +
"                            <td class=\"expander\"></td>\n" +
"                          </tr>\n" +
"                        </table>\n" +
"\n" +
"                      </td>\n" +
"                    </tr>\n" +
"                  </table>\n" +
"\n" +
"                </center>\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"          <table class=\"container\">\n" +
"            <tr>\n" +
"              <td>\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td>\n" +
"                            <h1>Hi, "+username+"</h1>\n" +
"                						<p class=\"lead\">You have successfully joined room "+roomName+" </p>\n" +
"                						<p>Please feel free to start chatting in room, after you   <a href='"+Configuration.serverPath+"login.xhtml"+"'>Log in</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row callout\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row footer\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"left-text-pad\">\n" +
"\n" +
"                            <h5>Connect With Us:</h5>\n" +
"\n" +
"                            <table class=\"tiny-button facebook\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Facebook</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button twitter\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Twitter</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button google-plus\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Google +</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"last right-text-pad\">\n" +
"                            <h5>Contact Info:</h5>\n" +
"                            <p>Phone: 408.341.0600</p>\n" +
"                            <p>Email: <a href=\"mailto:hseldon@trantor.com\">example@playchat.com</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td align=\"center\">\n" +
"                            <center>\n" +
"                              <p style=\"text-align:center;\"><a href=\"#\">Terms</a> | <a href=\"#\">Privacy</a> | <a href=\"#\">Unsubscribe</a></p>\n" +
"                            </center>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"              <!-- container end below -->\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"        </center>\n" +
"			</td>\n" +
"		</tr>\n" +
"	</table>\n" +
"</body>\n" +
"</html>";
    }  
       public static String bannMail (String username,String roomName,String UsernameBanned){
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"<head>\n" +
"	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"	<meta name=\"viewport\" content=\"width=device-width\"/>\n" +
"	<style>\n" +
"    \n" +
"  #outlook a { \n" +
"    padding:0; \n" +
"  } \n" +
"\n" +
"  body{ \n" +
"    width:100% !important; \n" +
"    min-width: 100%;\n" +
"    -webkit-text-size-adjust:100%; \n" +
"    -ms-text-size-adjust:100%; \n" +
"    margin:0; \n" +
"    padding:0;\n" +
"  }\n" +
"\n" +
"  .ExternalClass { \n" +
"    width:100%;\n" +
"  } \n" +
"\n" +
"  .ExternalClass, \n" +
"  .ExternalClass p, \n" +
"  .ExternalClass span, \n" +
"  .ExternalClass font, \n" +
"  .ExternalClass td, \n" +
"  .ExternalClass div { \n" +
"    line-height: 100%; \n" +
"  } \n" +
"\n" +
"  #backgroundTable { \n" +
"    margin:0; \n" +
"    padding:0; \n" +
"    width:100% !important; \n" +
"    line-height: 100% !important; \n" +
"  }\n" +
"\n" +
"  img { \n" +
"    outline:none; \n" +
"    text-decoration:none; \n" +
"    -ms-interpolation-mode: bicubic;\n" +
"    width: auto;\n" +
"    max-width: 100%; \n" +
"    float: left; \n" +
"    clear: both; \n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  center {\n" +
"    width: 100%;\n" +
"    min-width: 580px;\n" +
"  }\n" +
"\n" +
"  a img { \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  p {\n" +
"    margin: 0 0 0 10px;\n" +
"  }\n" +
"\n" +
"  table {\n" +
"    border-spacing: 0;\n" +
"    border-collapse: collapse;\n" +
"  }\n" +
"\n" +
"  td { \n" +
"    word-break: break-word;\n" +
"    -webkit-hyphens: auto;\n" +
"    -moz-hyphens: auto;\n" +
"    hyphens: auto;\n" +
"    border-collapse: collapse !important; \n" +
"  }\n" +
"\n" +
"  table, tr, td {\n" +
"    padding: 0;\n" +
"    vertical-align: top;\n" +
"    text-align: left;\n" +
"  }\n" +
"\n" +
"  hr {\n" +
"    color: #d9d9d9; \n" +
"    background-color: #d9d9d9; \n" +
"    height: 1px; \n" +
"    border: none;\n" +
"  }\n" +
"\n" +
"  /* Responsive Grid */\n" +
"\n" +
"  table.body {\n" +
"    height: 100%;\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  table.container {\n" +
"    width: 580px;\n" +
"    margin: 0 auto;\n" +
"    text-align: inherit;\n" +
"  }\n" +
"\n" +
"  table.row { \n" +
"    padding: 0px; \n" +
"    width: 100%;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.container table.row {\n" +
"    display: block;\n" +
"  }\n" +
"\n" +
"  td.wrapper {\n" +
"    padding: 10px 20px 0px 0px;\n" +
"    position: relative;\n" +
"  }\n" +
"\n" +
"  table.columns,\n" +
"  table.column {\n" +
"    margin: 0 auto;\n" +
"  }\n" +
"\n" +
"  table.columns td,\n" +
"  table.column td {\n" +
"    padding: 0px 0px 10px; \n" +
"  }\n" +
"\n" +
"  table.columns td.sub-columns,\n" +
"  table.column td.sub-columns,\n" +
"  table.columns td.sub-column,\n" +
"  table.column td.sub-column {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  td.sub-column, td.sub-columns {\n" +
"    min-width: 0px;\n" +
"  }\n" +
"\n" +
"  table.row td.last,\n" +
"  table.container td.last {\n" +
"    padding-right: 0px;\n" +
"  }\n" +
"\n" +
"  table.one { width: 30px; }\n" +
"  table.two { width: 80px; }\n" +
"  table.three { width: 130px; }\n" +
"  table.four { width: 180px; }\n" +
"  table.five { width: 230px; }\n" +
"  table.six { width: 280px; }\n" +
"  table.seven { width: 330px; }\n" +
"  table.eight { width: 380px; }\n" +
"  table.nine { width: 430px; }\n" +
"  table.ten { width: 480px; }\n" +
"  table.eleven { width: 530px; }\n" +
"  table.twelve { width: 580px; }\n" +
"\n" +
"  table.one center { min-width: 30px; }\n" +
"  table.two center { min-width: 80px; }\n" +
"  table.three center { min-width: 130px; }\n" +
"  table.four center { min-width: 180px; }\n" +
"  table.five center { min-width: 230px; }\n" +
"  table.six center { min-width: 280px; }\n" +
"  table.seven center { min-width: 330px; }\n" +
"  table.eight center { min-width: 380px; }\n" +
"  table.nine center { min-width: 430px; }\n" +
"  table.ten center { min-width: 480px; }\n" +
"  table.eleven center { min-width: 530px; }\n" +
"  table.twelve center { min-width: 580px; }\n" +
"\n" +
"  table.one .panel center { min-width: 10px; }\n" +
"  table.two .panel center { min-width: 60px; }\n" +
"  table.three .panel center { min-width: 110px; }\n" +
"  table.four .panel center { min-width: 160px; }\n" +
"  table.five .panel center { min-width: 210px; }\n" +
"  table.six .panel center { min-width: 260px; }\n" +
"  table.seven .panel center { min-width: 310px; }\n" +
"  table.eight .panel center { min-width: 360px; }\n" +
"  table.nine .panel center { min-width: 410px; }\n" +
"  table.ten .panel center { min-width: 460px; }\n" +
"  table.eleven .panel center { min-width: 510px; }\n" +
"  table.twelve .panel center { min-width: 560px; }\n" +
"\n" +
"  .body .columns td.one,\n" +
"  .body .column td.one { width: 8.333333%; }\n" +
"  .body .columns td.two,\n" +
"  .body .column td.two { width: 16.666666%; }\n" +
"  .body .columns td.three,\n" +
"  .body .column td.three { width: 25%; }\n" +
"  .body .columns td.four,\n" +
"  .body .column td.four { width: 33.333333%; }\n" +
"  .body .columns td.five,\n" +
"  .body .column td.five { width: 41.666666%; }\n" +
"  .body .columns td.six,\n" +
"  .body .column td.six { width: 50%; }\n" +
"  .body .columns td.seven,\n" +
"  .body .column td.seven { width: 58.333333%; }\n" +
"  .body .columns td.eight,\n" +
"  .body .column td.eight { width: 66.666666%; }\n" +
"  .body .columns td.nine,\n" +
"  .body .column td.nine { width: 75%; }\n" +
"  .body .columns td.ten,\n" +
"  .body .column td.ten { width: 83.333333%; }\n" +
"  .body .columns td.eleven,\n" +
"  .body .column td.eleven { width: 91.666666%; }\n" +
"  .body .columns td.twelve,\n" +
"  .body .column td.twelve { width: 100%; }\n" +
"\n" +
"  td.offset-by-one { padding-left: 50px; }\n" +
"  td.offset-by-two { padding-left: 100px; }\n" +
"  td.offset-by-three { padding-left: 150px; }\n" +
"  td.offset-by-four { padding-left: 200px; }\n" +
"  td.offset-by-five { padding-left: 250px; }\n" +
"  td.offset-by-six { padding-left: 300px; }\n" +
"  td.offset-by-seven { padding-left: 350px; }\n" +
"  td.offset-by-eight { padding-left: 400px; }\n" +
"  td.offset-by-nine { padding-left: 450px; }\n" +
"  td.offset-by-ten { padding-left: 500px; }\n" +
"  td.offset-by-eleven { padding-left: 550px; }\n" +
"\n" +
"  td.expander {\n" +
"    visibility: hidden;\n" +
"    width: 0px;\n" +
"    padding: 0 !important;\n" +
"  }\n" +
"\n" +
"  table.columns .text-pad,\n" +
"  table.column .text-pad {\n" +
"    padding-left: 10px;\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .left-text-pad,\n" +
"  table.columns .text-pad-left,\n" +
"  table.column .left-text-pad,\n" +
"  table.column .text-pad-left {\n" +
"    padding-left: 10px;\n" +
"  }\n" +
"\n" +
"  table.columns .right-text-pad,\n" +
"  table.columns .text-pad-right,\n" +
"  table.column .right-text-pad,\n" +
"  table.column .text-pad-right {\n" +
"    padding-right: 10px;\n" +
"  }\n" +
"\n" +
"  /* Block Grid */\n" +
"\n" +
"  .block-grid {\n" +
"    width: 100%;\n" +
"    max-width: 580px;\n" +
"  }\n" +
"\n" +
"  .block-grid td {\n" +
"    display: inline-block;\n" +
"    padding:10px;\n" +
"  }\n" +
"\n" +
"  .two-up td {\n" +
"    width:270px;\n" +
"  }\n" +
"\n" +
"  .three-up td {\n" +
"    width:173px;\n" +
"  }\n" +
"\n" +
"  .four-up td {\n" +
"    width:125px;\n" +
"  }\n" +
"\n" +
"  .five-up td {\n" +
"    width:96px;\n" +
"  }\n" +
"\n" +
"  .six-up td {\n" +
"    width:76px;\n" +
"  }\n" +
"\n" +
"  .seven-up td {\n" +
"    width:62px;\n" +
"  }\n" +
"\n" +
"  .eight-up td {\n" +
"    width:52px;\n" +
"  }\n" +
"\n" +
"  /* Alignment & Visibility Classes */\n" +
"\n" +
"  table.center, td.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  h1.center,\n" +
"  h2.center,\n" +
"  h3.center,\n" +
"  h4.center,\n" +
"  h5.center,\n" +
"  h6.center {\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  span.center {\n" +
"    display: block;\n" +
"    width: 100%;\n" +
"    text-align: center;\n" +
"  }\n" +
"\n" +
"  img.center {\n" +
"    margin: 0 auto;\n" +
"    float: none;\n" +
"  }\n" +
"\n" +
"  .show-for-small,\n" +
"  .hide-for-desktop {\n" +
"    display: none;\n" +
"  }\n" +
"\n" +
"  /* Typography */\n" +
"\n" +
"  body, table.body, h1, h2, h3, h4, h5, h6, p, td { \n" +
"    color: #222222;\n" +
"    font-family: \"Helvetica\", \"Arial\", sans-serif; \n" +
"    font-weight: normal; \n" +
"    padding:0; \n" +
"    margin: 0;\n" +
"    text-align: left; \n" +
"    line-height: 1.3;\n" +
"  }\n" +
"\n" +
"  h1, h2, h3, h4, h5, h6 {\n" +
"    word-break: normal;\n" +
"  }\n" +
"\n" +
"  h1 {font-size: 40px;}\n" +
"  h2 {font-size: 36px;}\n" +
"  h3 {font-size: 32px;}\n" +
"  h4 {font-size: 28px;}\n" +
"  h5 {font-size: 24px;}\n" +
"  h6 {font-size: 20px;}\n" +
"  body, table.body, p, td {font-size: 14px;line-height:19px;}\n" +
"\n" +
"  p.lead, p.lede, p.leed {\n" +
"    font-size: 18px;\n" +
"    line-height:21px;\n" +
"  }\n" +
"\n" +
"  p { \n" +
"    margin-bottom: 10px;\n" +
"  }\n" +
"\n" +
"  small {\n" +
"    font-size: 10px;\n" +
"  }\n" +
"\n" +
"  a {\n" +
"    color: #2ba6cb; \n" +
"    text-decoration: none;\n" +
"  }\n" +
"\n" +
"  a:hover { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:active { \n" +
"    color: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  a:visited { \n" +
"    color: #2ba6cb !important;\n" +
"  }\n" +
"\n" +
"  h1 a, \n" +
"  h2 a, \n" +
"  h3 a, \n" +
"  h4 a, \n" +
"  h5 a, \n" +
"  h6 a {\n" +
"    color: #2ba6cb;\n" +
"  }\n" +
"\n" +
"  h1 a:active, \n" +
"  h2 a:active,  \n" +
"  h3 a:active, \n" +
"  h4 a:active, \n" +
"  h5 a:active, \n" +
"  h6 a:active { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  h1 a:visited, \n" +
"  h2 a:visited,  \n" +
"  h3 a:visited, \n" +
"  h4 a:visited, \n" +
"  h5 a:visited, \n" +
"  h6 a:visited { \n" +
"    color: #2ba6cb !important; \n" +
"  } \n" +
"\n" +
"  /* Panels */\n" +
"\n" +
"  .panel {\n" +
"    background: #f2f2f2;\n" +
"    border: 1px solid #d9d9d9;\n" +
"    padding: 10px !important;\n" +
"  }\n" +
"\n" +
"  .sub-grid table {\n" +
"    width: 100%;\n" +
"  }\n" +
"\n" +
"  .sub-grid td.sub-columns {\n" +
"    padding-bottom: 0;\n" +
"  }\n" +
"\n" +
"  /* Buttons */\n" +
"\n" +
"  table.button,\n" +
"  table.tiny-button,\n" +
"  table.small-button,\n" +
"  table.medium-button,\n" +
"  table.large-button {\n" +
"    width: 100%;\n" +
"    overflow: hidden;\n" +
"  }\n" +
"\n" +
"  table.button td,\n" +
"  table.tiny-button td,\n" +
"  table.small-button td,\n" +
"  table.medium-button td,\n" +
"  table.large-button td {\n" +
"    display: block;\n" +
"    width: auto !important;\n" +
"    text-align: center;\n" +
"    background: #2ba6cb;\n" +
"    border: 1px solid #2284a1;\n" +
"    color: #ffffff;\n" +
"    padding: 8px 0;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td {\n" +
"    padding: 5px 0 4px;\n" +
"  }\n" +
"\n" +
"  table.small-button td {\n" +
"    padding: 8px 0 7px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td {\n" +
"    padding: 12px 0 10px;\n" +
"  }\n" +
"\n" +
"  table.large-button td {\n" +
"    padding: 21px 0 18px;\n" +
"  }\n" +
"\n" +
"  table.button td a,\n" +
"  table.tiny-button td a,\n" +
"  table.small-button td a,\n" +
"  table.medium-button td a,\n" +
"  table.large-button td a {\n" +
"    font-weight: bold;\n" +
"    text-decoration: none;\n" +
"    font-family: Helvetica, Arial, sans-serif;\n" +
"    color: #ffffff;\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.tiny-button td a {\n" +
"    font-size: 12px;\n" +
"    font-weight: normal;\n" +
"  }\n" +
"\n" +
"  table.small-button td a {\n" +
"    font-size: 16px;\n" +
"  }\n" +
"\n" +
"  table.medium-button td a {\n" +
"    font-size: 20px;\n" +
"  }\n" +
"\n" +
"  table.large-button td a {\n" +
"    font-size: 24px;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.button:visited td,\n" +
"  table.button:active td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:visited td a,\n" +
"  table.button:active td a {\n" +
"    color: #fff !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td,\n" +
"  table.tiny-button:hover td,\n" +
"  table.small-button:hover td,\n" +
"  table.medium-button:hover td,\n" +
"  table.large-button:hover td {\n" +
"    background: #2795b6 !important;\n" +
"  }\n" +
"\n" +
"  table.button:hover td a,\n" +
"  table.button:active td a,\n" +
"  table.button td a:visited,\n" +
"  table.tiny-button:hover td a,\n" +
"  table.tiny-button:active td a,\n" +
"  table.tiny-button td a:visited,\n" +
"  table.small-button:hover td a,\n" +
"  table.small-button:active td a,\n" +
"  table.small-button td a:visited,\n" +
"  table.medium-button:hover td a,\n" +
"  table.medium-button:active td a,\n" +
"  table.medium-button td a:visited,\n" +
"  table.large-button:hover td a,\n" +
"  table.large-button:active td a,\n" +
"  table.large-button td a:visited {\n" +
"    color: #ffffff !important; \n" +
"  }\n" +
"\n" +
"  table.secondary td {\n" +
"    background: #e9e9e9;\n" +
"    border-color: #d0d0d0;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary td a {\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td {\n" +
"    background: #d0d0d0 !important;\n" +
"    color: #555;\n" +
"  }\n" +
"\n" +
"  table.secondary:hover td a,\n" +
"  table.secondary td a:visited,\n" +
"  table.secondary:active td a {\n" +
"    color: #555 !important;\n" +
"  }\n" +
"\n" +
"  table.success td {\n" +
"    background: #5da423;\n" +
"    border-color: #457a1a;\n" +
"  }\n" +
"\n" +
"  table.success:hover td {\n" +
"    background: #457a1a !important;\n" +
"  }\n" +
"\n" +
"  table.alert td {\n" +
"    background: #c60f13;\n" +
"    border-color: #970b0e;\n" +
"  }\n" +
"\n" +
"  table.alert:hover td {\n" +
"    background: #970b0e !important;\n" +
"  }\n" +
"\n" +
"  table.radius td {\n" +
"    -webkit-border-radius: 3px;\n" +
"    -moz-border-radius: 3px;\n" +
"    border-radius: 3px;\n" +
"  }\n" +
"\n" +
"  table.round td {\n" +
"    -webkit-border-radius: 500px;\n" +
"    -moz-border-radius: 500px;\n" +
"    border-radius: 500px;\n" +
"  }\n" +
"\n" +
"  /* Outlook First */\n" +
"\n" +
"  body.outlook p {\n" +
"    display: inline !important;\n" +
"  }\n" +
"\n" +
"  /*  Media Queries */\n" +
"\n" +
"  @media only screen and (max-width: 600px) {\n" +
"\n" +
"    table[class=\"body\"] img {\n" +
"      width: auto !important;\n" +
"      height: auto !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] center {\n" +
"      min-width: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .container {\n" +
"      width: 95% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .row {\n" +
"      width: 100% !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper {\n" +
"      display: block !important;\n" +
"      padding-right: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns,\n" +
"    table[class=\"body\"] .column {\n" +
"      table-layout: fixed !important;\n" +
"      float: none !important;\n" +
"      width: 100% !important;\n" +
"      padding-right: 0px !important;\n" +
"      padding-left: 0px !important;\n" +
"      display: block !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .wrapper.first .columns,\n" +
"    table[class=\"body\"] .wrapper.first .column {\n" +
"      display: table !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td,\n" +
"    table[class=\"body\"] table.column td {\n" +
"      width: 100% !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .columns td.one,\n" +
"    table[class=\"body\"] .column td.one { width: 8.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.two,\n" +
"    table[class=\"body\"] .column td.two { width: 16.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.three,\n" +
"    table[class=\"body\"] .column td.three { width: 25% !important; }\n" +
"    table[class=\"body\"] .columns td.four,\n" +
"    table[class=\"body\"] .column td.four { width: 33.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.five,\n" +
"    table[class=\"body\"] .column td.five { width: 41.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.six,\n" +
"    table[class=\"body\"] .column td.six { width: 50% !important; }\n" +
"    table[class=\"body\"] .columns td.seven,\n" +
"    table[class=\"body\"] .column td.seven { width: 58.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eight,\n" +
"    table[class=\"body\"] .column td.eight { width: 66.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.nine,\n" +
"    table[class=\"body\"] .column td.nine { width: 75% !important; }\n" +
"    table[class=\"body\"] .columns td.ten,\n" +
"    table[class=\"body\"] .column td.ten { width: 83.333333% !important; }\n" +
"    table[class=\"body\"] .columns td.eleven,\n" +
"    table[class=\"body\"] .column td.eleven { width: 91.666666% !important; }\n" +
"    table[class=\"body\"] .columns td.twelve,\n" +
"    table[class=\"body\"] .column td.twelve { width: 100% !important; }\n" +
"\n" +
"    table[class=\"body\"] td.offset-by-one,\n" +
"    table[class=\"body\"] td.offset-by-two,\n" +
"    table[class=\"body\"] td.offset-by-three,\n" +
"    table[class=\"body\"] td.offset-by-four,\n" +
"    table[class=\"body\"] td.offset-by-five,\n" +
"    table[class=\"body\"] td.offset-by-six,\n" +
"    table[class=\"body\"] td.offset-by-seven,\n" +
"    table[class=\"body\"] td.offset-by-eight,\n" +
"    table[class=\"body\"] td.offset-by-nine,\n" +
"    table[class=\"body\"] td.offset-by-ten,\n" +
"    table[class=\"body\"] td.offset-by-eleven {\n" +
"      padding-left: 0 !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] table.columns td.expander {\n" +
"      width: 1px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .right-text-pad,\n" +
"    table[class=\"body\"] .text-pad-right {\n" +
"      padding-left: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .left-text-pad,\n" +
"    table[class=\"body\"] .text-pad-left {\n" +
"      padding-right: 10px !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .hide-for-small,\n" +
"    table[class=\"body\"] .show-for-desktop {\n" +
"      display: none !important;\n" +
"    }\n" +
"\n" +
"    table[class=\"body\"] .show-for-small,\n" +
"    table[class=\"body\"] .hide-for-desktop {\n" +
"      display: inherit !important;\n" +
"    }\n" +
"  }\n" +
"\n" +
"    </style>\n" +
"    <style>\n" +
"\n" +
"      table.facebook td {\n" +
"        background: #3b5998;\n" +
"        border-color: #2d4473;\n" +
"      }\n" +
"\n" +
"      table.facebook:hover td {\n" +
"        background: #2d4473 !important;\n" +
"      }\n" +
"\n" +
"      table.twitter td {\n" +
"        background: #00acee;\n" +
"        border-color: #0087bb;\n" +
"      }\n" +
"\n" +
"      table.twitter:hover td {\n" +
"        background: #0087bb !important;\n" +
"      }\n" +
"\n" +
"      table.google-plus td {\n" +
"        background-color: #DB4A39;\n" +
"        border-color: #CC0000;\n" +
"      }\n" +
"\n" +
"      table.google-plus:hover td {\n" +
"        background: #CC0000 !important;\n" +
"      }\n" +
"\n" +
"      .template-label {\n" +
"        color: #ffffff;\n" +
"        font-weight: bold;\n" +
"        font-size: 11px;\n" +
"      }\n" +
"\n" +
"      .callout .panel {\n" +
"        background: #ECF8FF;\n" +
"        border-color: #b9e5ff;\n" +
"      }\n" +
"\n" +
"      .header {\n" +
"        background: #999999;\n" +
"      }\n" +
"\n" +
"      .footer .wrapper {\n" +
"        background: #ebebeb;\n" +
"      }\n" +
"\n" +
"      .footer h5 {\n" +
"        padding-bottom: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .text-pad {\n" +
"        padding-left: 10px;\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .left-text-pad {\n" +
"        padding-left: 10px;\n" +
"      }\n" +
"\n" +
"      table.columns .right-text-pad {\n" +
"        padding-right: 10px;\n" +
"      }\n" +
"\n" +
"      @media only screen and (max-width: 600px) {\n" +
"\n" +
"        table[class=\"body\"] .right-text-pad {\n" +
"          padding-left: 10px !important;\n" +
"        }\n" +
"\n" +
"        table[class=\"body\"] .left-text-pad {\n" +
"          padding-right: 10px !important;\n" +
"        }\n" +
"      }\n" +
"  </style>\n" +
"  <style>\n" +
"\n" +
"    table.facebook td {\n" +
"      background: #3b5998;\n" +
"      border-color: #2d4473;\n" +
"    }\n" +
"\n" +
"    table.facebook:hover td {\n" +
"      background: #2d4473 !important;\n" +
"    }\n" +
"\n" +
"    table.twitter td {\n" +
"      background: #00acee;\n" +
"      border-color: #0087bb;\n" +
"    }\n" +
"\n" +
"    table.twitter:hover td {\n" +
"      background: #0087bb !important;\n" +
"    }\n" +
"\n" +
"    table.google-plus td {\n" +
"      background-color: #DB4A39;\n" +
"      border-color: #CC0000;\n" +
"    }\n" +
"\n" +
"    table.google-plus:hover td {\n" +
"      background: #CC0000 !important;\n" +
"    }\n" +
"\n" +
"    .template-label {\n" +
"      color: #ffffff;\n" +
"      font-weight: bold;\n" +
"      font-size: 11px;\n" +
"    }\n" +
"\n" +
"    .callout .wrapper {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"\n" +
"    .callout .panel {\n" +
"      background: #ECF8FF;\n" +
"      border-color: #b9e5ff;\n" +
"    }\n" +
"\n" +
"    .header {\n" +
"      background: #999999;\n" +
"    }\n" +
"\n" +
"    .footer .wrapper {\n" +
"      background: #ebebeb;\n" +
"    }\n" +
"\n" +
"    .footer h5 {\n" +
"      padding-bottom: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .text-pad {\n" +
"      padding-left: 10px;\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .left-text-pad {\n" +
"      padding-left: 10px;\n" +
"    }\n" +
"\n" +
"    table.columns .right-text-pad {\n" +
"      padding-right: 10px;\n" +
"    }\n" +
"\n" +
"    @media only screen and (max-width: 600px) {\n" +
"\n" +
"      table[class=\"body\"] .right-text-pad {\n" +
"        padding-left: 10px !important;\n" +
"      }\n" +
"\n" +
"      table[class=\"body\"] .left-text-pad {\n" +
"        padding-right: 10px !important;\n" +
"      }\n" +
"    }\n" +
"\n" +
"	</style>\n" +
"</head>\n" +
"<body>\n" +
"	<table class=\"body\">\n" +
"		<tr>\n" +
"			<td class=\"center\" align=\"center\" valign=\"top\">\n" +
"        <center>\n" +
"\n" +
"          <table class=\"row header\">\n" +
"            <tr>\n" +
"              <td class=\"center\" align=\"center\">\n" +
"                <center>\n" +
"\n" +
"                  <table class=\"container\">\n" +
"                    <tr>\n" +
"                      <td class=\"wrapper last\">\n" +
"\n" +
"                        <table class=\"twelve columns\">\n" +
"                          <tr>\n" +
"                            <td class=\"six sub-columns\">\n" +
"                              <h1 style=\"color: #009EC3; text-shadow:1px 1px black;\">PlayChat</h1>\n" +
"                            </td>\n" +
"                            <td class=\"six sub-columns last\" style=\"text-align:right; vertical-align:middle;\">\n" +
"                              <span class=\"template-label\" style=\"font-size: 15px;\">Join room</span>\n" +
"                            </td>\n" +
"                            <td class=\"expander\"></td>\n" +
"                          </tr>\n" +
"                        </table>\n" +
"\n" +
"                      </td>\n" +
"                    </tr>\n" +
"                  </table>\n" +
"\n" +
"                </center>\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"          <table class=\"container\">\n" +
"            <tr>\n" +
"              <td>\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td>\n" +
"                            <h1>Hi, "+username+"</h1>\n" +
"                						<p class=\"lead\">You have been banned from room "+roomName+", by a user "+UsernameBanned+" </p>\n" +           						
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row callout\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"                <table class=\"row footer\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"left-text-pad\">\n" +
"\n" +
"                            <h5>Connect With Us:</h5>\n" +
"\n" +
"                            <table class=\"tiny-button facebook\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Facebook</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button twitter\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Twitter</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                            <br>\n" +
"\n" +
"                            <table class=\"tiny-button google-plus\">\n" +
"                              <tr>\n" +
"                                <td>\n" +
"                                  <a href=\"#\">Google +</a>\n" +
"                                </td>\n" +
"                              </tr>\n" +
"                            </table>\n" +
"\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"six columns\">\n" +
"                        <tr>\n" +
"                          <td class=\"last right-text-pad\">\n" +
"                            <h5>Contact Info:</h5>\n" +
"                            <p>Phone: 408.341.0600</p>\n" +
"                            <p>Email: <a href=\"mailto:hseldon@trantor.com\">example@playchat.com</a></p>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"\n" +
"                <table class=\"row\">\n" +
"                  <tr>\n" +
"                    <td class=\"wrapper last\">\n" +
"\n" +
"                      <table class=\"twelve columns\">\n" +
"                        <tr>\n" +
"                          <td align=\"center\">\n" +
"                            <center>\n" +
"                              <p style=\"text-align:center;\"><a href=\"#\">Terms</a> | <a href=\"#\">Privacy</a> | <a href=\"#\">Unsubscribe</a></p>\n" +
"                            </center>\n" +
"                          </td>\n" +
"                          <td class=\"expander\"></td>\n" +
"                        </tr>\n" +
"                      </table>\n" +
"\n" +
"                    </td>\n" +
"                  </tr>\n" +
"                </table>\n" +
"\n" +
"              <!-- container end below -->\n" +
"              </td>\n" +
"            </tr>\n" +
"          </table>\n" +
"\n" +
"        </center>\n" +
"			</td>\n" +
"		</tr>\n" +
"	</table>\n" +
"</body>\n" +
"</html>";
    }  
}
