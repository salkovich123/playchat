/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.mail;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author RokSket
 */
public abstract class Mail {
    
    private String from = "salej7@gmail.com" ;
    String fromName = "Testiranje mailov";
   
    
    String to = "";
    Properties props;
    private Session session;
    
    String subject = "";
    String content = "";
    
    public Mail(){
        
    }
    
  
    
    public void loadProperties(){
     final String username = "salej7@gmail.com  ";
		final String password = "marcel14";

	 props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		 session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
                        @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
    }
    
    public MimeMessage setMessage()
    {
        MimeMessage message = new MimeMessage(getSession());

         try
         {
           try{
                message.setFrom(new InternetAddress(getFrom(), fromName));
           }
           catch(Exception e){}
           message.addRecipients(Message.RecipientType.BCC, getFrom());
           message.addRecipients(Message.RecipientType.TO,to);
           message.setSubject(subject, "text/html; charset=UTF-8");
           message.setHeader("Content-Type", "UTF-8");
           message.setContent(content, "text/html; charset=UTF-8");

         }
         catch (MessagingException ex)
         {
             System.out.println("Error sending message....");
             Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
         }

         return message; 
    }
    
  
    //send mail to one or more users
    public void sendMail() throws MessagingException
    {
        loadProperties();
        
          //set receipent
           Transport.send(setMessage());
           System.out.println("Sent message successfully....");
   
    }

  

   

    /**
     * @return the session
     */
    public Session getSession() {
        return session;
    }

    /**
     * @return the host
     */
    

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }
    
    
    
    
    
    

   
    

}
