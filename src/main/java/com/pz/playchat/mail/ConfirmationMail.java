/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.mail;

import java.io.Serializable;
import javax.ejb.Stateless;

/**
 *
 * @author Marcelino
 */

public class ConfirmationMail extends Mail  {
    
    private String fromName = "PlayChat ";
    private String subject = "Registration confirmation mail";
    
    public ConfirmationMail(){
        super();
        super.fromName = fromName;
        super.subject = subject;
        
    }
        public void setData(String to, String username, String url) throws Exception{
        
        if(to == "" || to == null){
           throw new Exception("Prazni podatki! Napolnite jih z metodo setData!");
        }
        super.to = to;
        super.content = Templates.confirmationEmail(username, url);     
    }
    
}
