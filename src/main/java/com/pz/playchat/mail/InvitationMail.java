/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.mail;

/**
 *
 * @author Marcelino
 */
public class InvitationMail extends Mail {
      private String fromName = "PlayChat ";
    private String subject = "Invitation to chat group";

    
    public InvitationMail(){
          super();
        super.fromName = fromName;
        super.subject = subject;
        
    }
     public void setData(String to,String invitator, String username, String url,String roomName) throws Exception{
        
        if(to == "" || to == null){
           throw new Exception("Prazni podatki! Napolnite jih z metodo setData!");
        }
        super.to = to;
        super.content = Templates.sendInvite(username,invitator, url, roomName);     
    }
}
