// Package
package com.pz.playchat.utils;

// Imported classes
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * A class containing helpful for easier JSF development
 * @author Marcelino/LeonDobnik
 */
public class JsfUtils
{
  /**
   * FacesMessage identifier for fatal error message
   */
  public static final FacesMessage.Severity SEVERITY_FATAL = FacesMessage.SEVERITY_FATAL;

  /**
   * FacesMessage identifier for error message
   */
  public static final FacesMessage.Severity SEVERITY_ERROR = FacesMessage.SEVERITY_ERROR;
 
  /**
   * FacesMessage identifier for warning message
   */
  public static final FacesMessage.Severity SEVERITY_WARN = FacesMessage.SEVERITY_WARN;
  
  /**
   * FacesMessage identifier for fatal information message
   */
  public static final FacesMessage.Severity SEVERITY_INFO = FacesMessage.SEVERITY_INFO;
  
  /**
   * Constructs a FacesMessage and appends it to a set of messages which are assumed 
   * not be associated with any specific component instance<br />
   * Original documentation states: Append a FacesMessage to the set of messages associated 
   * with the specified client identifier, if clientId is not null. If clientId is null, 
   * this FacesMessage is assumed to not be associated with any specific component instance.
   * @param severity type of message (fatal, error, warning, information)
   * @param summary short description of a message
   * @param detail detailed description of a message
   * @throws IllegalStateException - if this method is called after this instance has been released
   * @throws NullPointerException - if message is null
   * @see FacesMessage
   * @see FacesContext
   */
  public static void addMessage(FacesMessage.Severity severity, String summary, String detail)
  {
    addMessage(severity, summary, detail, null);
  }
  
  /**
   * Constructs FacesMessage and appends a FacesMessage to the set of messages associated 
   * with the specified client identifier, if clientId is not null. If clientId is null, 
   * this FacesMessage is assumed to not be associated with any specific component instance.
   * @param severity type of message (fatal, error, warning, information)
   * @param summary short description of a message
   * @param detail detailed description of a message
   * @param clientId the client identifier with which this message is associated (if any)
   * @throws IllegalStateException if this method is called after this instance has been released
   * @throws NullPointerException if message is null
   * @see FacesMessage
   * @see FacesContext
   */
  public static void addMessage(FacesMessage.Severity severity, String summary, String detail, String clientId)
  {
    FacesMessage fm = new FacesMessage(severity, summary, detail);
    FacesContext.getCurrentInstance().addMessage(clientId, fm); 
  }
  
  /**
   * Returns current instance of FacesContext by calling 
   * <code>FacesContext.getCurrentInstance()</code><br />
   * <strong>Original documentation states:</strong> <i>Return the FacesContext instance for 
   * the request that is being processed by the current thread. If called during 
   * application initialization or shutdown, any method documented as "valid 
   * to call this method during application startup or shutdown" must be 
   * supported during application startup or shutdown time. The result of 
   * calling a method during application startup or shutdown time that does not 
   * have this designation is undefined.</i>
   * @return FacesContext instance if available
   * @see FacesContext
   */
  public static FacesContext getCurrentFacesContext()
  {
    return FacesContext.getCurrentInstance();
  }
  
  /**
   * Returns current instance of ExternalContext by calling 
   * <code>FacesContext.getCurrentInstance().getExternalContext()</code><br />
   * Original documentation states: <i>Return the ExternalContext instance for 
   * this FacesContext instance. <br />
   * It is valid to call this method during application startup or shutdown. If 
   * called during application startup or shutdown, this method returns an 
   * ExternalContext instance with the special behaviors indicated in the 
   * javadoc for that class. Methods document as being valid to call during 
   * application startup or shutdown must be supported.</i>
   * @return ExternalContext instance, if current FacesContext is null returns null
   * @throws IllegalStateException if this method is called after this instance has been released<
   * @see FacesContext
   * @see ExternalContext
   */
  public static ExternalContext getCurrentExternalContext()
  {
    ExternalContext externalContext = null;
    FacesContext facesContext = FacesContext.getCurrentInstance();
    
    if (facesContext != null)
    {
      externalContext = facesContext.getExternalContext();
    }
    
    return externalContext;
  }
}