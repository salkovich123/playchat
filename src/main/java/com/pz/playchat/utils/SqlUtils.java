// Package
package com.pz.playchat.utils;

// Imported classes
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains various helper functions for working with java <code>java.sql</code>
 * package. All closeQuietly methods does not throw SQLException - but it can be detected
 * on trace level of logger settings
 * @author Leon Dobnik
 */
public class SqlUtils
{
  /**
   * Logger of this class
   */
  private static final Logger logger = LoggerFactory.getLogger(SqlUtils.class);
  
  /**
   * Closes Connection without throwing an exception
   * @param connection to close
   * @see java.sql.Connection
   */
  public static void closeQuietly(Connection connection)
  {
    if (connection != null)
    {
      try
      {
        connection.close();
      }
      
      catch(SQLException e)
      {
        logger.trace("Failed to close " + Connection.class.getCanonicalName() + " quietly!", e);
      }
    }
  }
  
  /**
   * Closes Statement without throwing an exception
   * @param statement to close
   * @see java.sql.Statement
   */
  public static void closeQuietly(Statement statement)
  {
    if (statement != null)
    {
      try
      {
        statement.close();
      }
      
      catch(SQLException e)
      {
        logger.trace("Failed to close " + Statement.class.getCanonicalName() + " quietly!", e);
      }
    }
  }
  
  /**
   * Closes ResultSet without throwing an exception
   * @param resultSet to close
   * @see java.sql.ResultSet
   */
  public static void closeQuietly(ResultSet resultSet)
  {
    if (resultSet != null)
    {
      try
      {
        resultSet.close();
      }
      
      catch(SQLException e)
      {
        logger.trace("Failed to close " + ResultSet.class.getCanonicalName() + " quietly!", e);
      }
    }
  }
  
  /**
   * Closes Connection, PreparedStatement and ResultSet without throwing an exception
   * @param connection to close
   * @param preparedStatement to close
   * @param resultSet to close
   */
  public static void closeQuietly(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet)
  {
    closeQuietly(resultSet);
    closeQuietly(preparedStatement);
    closeQuietly(connection);
  }
}
