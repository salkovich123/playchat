/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcelino
 */
@Entity
@Table(name = "User")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByIdUser", query = "SELECT u FROM User u WHERE u.idUser = :idUser"),
    @NamedQuery(name = "User.findByUserfirstname", query = "SELECT u FROM User u WHERE u.userfirstname = :userfirstname"),
    @NamedQuery(name = "User.findByUserlastname", query = "SELECT u FROM User u WHERE u.userlastname = :userlastname"),
    @NamedQuery(name = "User.findByUserusername", query = "SELECT u FROM User u WHERE u.userusername = :userusername"),
    @NamedQuery(name = "User.findByUseruniqueId", query = "SELECT u FROM User u WHERE u.useruniqueId = :useruniqueId"),
    @NamedQuery(name = "User.findByUseremail", query = "SELECT u FROM User u WHERE u.useremail = :useremail"),
    @NamedQuery(name = "User.findByUserpassword", query = "SELECT u FROM User u WHERE u.userpassword = :userpassword"),
    @NamedQuery(name = "User.findByUsercreateTime", query = "SELECT u FROM User u WHERE u.usercreateTime = :usercreateTime"),
    @NamedQuery(name = "User.findByUserloginTime", query = "SELECT u FROM User u WHERE u.userloginTime = :userloginTime"),
    @NamedQuery(name = "User.findByUserconfirmed", query = "SELECT u FROM User u WHERE u.userconfirmed = :userconfirmed"),
    @NamedQuery(name = "User.findByUserimagePath", query = "SELECT u FROM User u WHERE u.userimagePath = :userimagePath"),
    @NamedQuery(name = "User.findByUsersaltIteration", query = "SELECT u FROM User u WHERE u.usersaltIteration = :usersaltIteration"),
    @NamedQuery(name = "User.findByloginQuery", query = "SELECT u.userpassword, u.userpasswordSalt, u.usersaltIteration, u.userconfirmed,u.idUser FROM User u WHERE  u.userusername= :userusername")})
    
public class User implements Serializable {
     private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUser")
    private Integer idUser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "User_firstname")
    private String userfirstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "User_lastname")
    private String userlastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "User_username")
    private String userusername;
    @Size(max = 200)
    @Column(name = "User_uniqueId")
    private String useruniqueId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "User_email")
    private String useremail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "User_password")
    private String userpassword;
    @Basic(optional = false)
    @NotNull
    @Column(name = "User_createTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date usercreateTime;
    @Column(name = "User_loginTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date userloginTime;
    @Column(name = "User_confirmed")
    private Boolean userconfirmed;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "User_imagePath")
    private String userimagePath;
    @Basic(optional = false)
    @NotNull
    @Column(name = "User_saltIteration")
    private int usersaltIteration;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "User_passwordSalt")
    private byte[] userpasswordSalt;
     @JoinColumn(name = "Role_idRole", referencedColumnName = "idRole")
    @ManyToOne(optional = false)
    private Role roleidRole;
        @Column(name = "User_loggedIn")
    private Boolean loggedIn;

    public User() {
    }

    public User(Integer idUser) {
        this.idUser = idUser;
    }

    public User(Integer idUser, String userfirstname, String userlastname, String userusername, String useremail, String userpassword, Date usercreateTime, String userimagePath, int usersaltIteration, byte[] userpasswordSalt) {
        this.idUser = idUser;
        this.userfirstname = userfirstname;
        this.userlastname = userlastname;
        this.userusername = userusername;
        this.useremail = useremail;
        this.userpassword = userpassword;
        this.usercreateTime = usercreateTime;
        this.userimagePath = userimagePath;
        this.usersaltIteration = usersaltIteration;
        this.userpasswordSalt = userpasswordSalt;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUserfirstname() {
        return userfirstname;
    }

    public void setUserfirstname(String userfirstname) {
        this.userfirstname = userfirstname;
    }

    public String getUserlastname() {
        return userlastname;
    }

    public void setUserlastname(String userlastname) {
        this.userlastname = userlastname;
    }

    public String getUserusername() {
        return userusername;
    }

    public void setUserusername(String userusername) {
        this.userusername = userusername;
    }

    public String getUseruniqueId() {
        return useruniqueId;
    }

    public void setUseruniqueId(String useruniqueId) {
        this.useruniqueId = useruniqueId;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public Date getUsercreateTime() {
        return usercreateTime;
    }

    public void setUsercreateTime(Date usercreateTime) {
        this.usercreateTime = usercreateTime;
    }

    public Date getUserloginTime() {
        return userloginTime;
    }

    public void setUserloginTime(Date userloginTime) {
        this.userloginTime = userloginTime;
    }

    public Boolean getUserconfirmed() {
        return userconfirmed;
    }

    public void setUserconfirmed(Boolean userconfirmed) {
        this.userconfirmed = userconfirmed;
    }

    public String getUserimagePath() {
        return userimagePath;
    }

    public void setUserimagePath(String userimagePath) {
        this.userimagePath = userimagePath;
    }

    public int getUsersaltIteration() {
        return usersaltIteration;
    }

    public void setUsersaltIteration(int usersaltIteration) {
        this.usersaltIteration = usersaltIteration;
    }

    public byte[] getUserpasswordSalt() {
        return userpasswordSalt;
    }

    public void setUserpasswordSalt(byte[] userpasswordSalt) {
        this.userpasswordSalt = userpasswordSalt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    public Role getRoleidRole() {
        return roleidRole;
    }

    public void setRoleidRole(Role roleidRole) {
        this.roleidRole = roleidRole;
    }

    @Override
    public String toString() {
        return "com.test.User[ idUser=" + idUser + " ]";
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
}
