/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcelino
 */
@Entity
@Table(name = "ChatRoomUsers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChatRoomUsers.findAll", query = "SELECT c FROM ChatRoomUsers c"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoomUsersId", query = "SELECT c FROM ChatRoomUsers c WHERE c.chatRoomUsersId = :chatRoomUsersId"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoomUsersconfirmed", query = "SELECT c FROM ChatRoomUsers c WHERE c.chatRoomUsersconfirmed = :chatRoomUsersconfirmed"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoomUsersRoom", query = "SELECT c FROM ChatRoomUsers c WHERE c.chatRoomidChatRoom = :chatRoom AND c.useruserId = :user"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoom", query = "SELECT c FROM ChatRoomUsers c WHERE c.chatRoomidChatRoom = :chatRoom"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoomUsersID", query = "SELECT c FROM ChatRoomUsers c WHERE c.useruserId = :userID and c.chatRoomUsersconfirmed = true"),
    @NamedQuery(name = "ChatRoomUsers.findByChatRoomUsersJoinedDate", query = "SELECT c FROM ChatRoomUsers c WHERE c.chatRoomUsersJoinedDate = :chatRoomUsersJoinedDate")})
public class ChatRoomUsers implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idChatRoomUsers")
    private Integer chatRoomUsersId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ChatRoomUsers_confirmed")
    private boolean chatRoomUsersconfirmed;
  
    @Column(name = "ChatRoomUsers_joinedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date chatRoomUsersJoinedDate;
    @JoinColumn(name = "ChatRoom_idChatRoom", referencedColumnName = "idChatRoom")
    @ManyToOne(optional = false)
    private ChatRoom chatRoomidChatRoom;
    @JoinColumn(name = "User_idUser", referencedColumnName = "idUser")
    @ManyToOne(optional = false)
    private User useruserId;

    public ChatRoomUsers() {
    }

    public ChatRoomUsers(Integer chatRoomUsersId) {
        this.chatRoomUsersId = chatRoomUsersId;
    }

    public ChatRoomUsers(Integer chatRoomUsersId, boolean chatRoomUsersconfirmed) {
        this.chatRoomUsersId = chatRoomUsersId;
        this.chatRoomUsersconfirmed = chatRoomUsersconfirmed;
    }

    public Integer getChatRoomUsersId() {
        return chatRoomUsersId;
    }

    public void setChatRoomUsersId(Integer chatRoomUsersId) {
        this.chatRoomUsersId = chatRoomUsersId;
    }

    public boolean getChatRoomUsersconfirmed() {
        return chatRoomUsersconfirmed;
    }

    public void setChatRoomUsersconfirmed(boolean chatRoomUsersconfirmed) {
        this.chatRoomUsersconfirmed = chatRoomUsersconfirmed;
    }

    public Date getChatRoomUsersJoinedDate() {
        return chatRoomUsersJoinedDate;
    }

    public void setChatRoomUsersJoinedDate(Date chatRoomUsersJoinedDate) {
        this.chatRoomUsersJoinedDate = chatRoomUsersJoinedDate;
    }

    public ChatRoom getChatRoomidChatRoom() {
        return chatRoomidChatRoom;
    }

    public void setChatRoomidChatRoom(ChatRoom chatRoomidChatRoom) {
        this.chatRoomidChatRoom = chatRoomidChatRoom;
    }

    public User getUseruserId() {
        return useruserId;
    }

    public void setUseruserId(User useruserId) {
        this.useruserId = useruserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chatRoomUsersId != null ? chatRoomUsersId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChatRoomUsers)) {
            return false;
        }
        ChatRoomUsers other = (ChatRoomUsers) object;
        if ((this.chatRoomUsersId == null && other.chatRoomUsersId != null) || (this.chatRoomUsersId != null && !this.chatRoomUsersId.equals(other.chatRoomUsersId))) {
            return false;
        }
        return true;
    }

   
    @Override
    public String toString() {
        return "com.pz.playchat.chat.entity.ChatRoomUsers[ chatRoomUsersId=" + chatRoomUsersId + " ]";
    }
    
}
