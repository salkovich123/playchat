/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marcelino
 */
@Entity
@Table(name = "ChatRoom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChatRoom.findAll", query = "SELECT c FROM ChatRoom c"),
    @NamedQuery(name = "ChatRoom.findByIdChatRoom", query = "SELECT c FROM ChatRoom c WHERE c.idChatRoom = :idChatRoom"),
    @NamedQuery(name = "ChatRoom.findByChatRoomname", query = "SELECT c FROM ChatRoom c WHERE c.chatRoomname = :chatRoomname"),
     @NamedQuery(name = "ChatRoom.findByChatRoomUniqueId", query = "SELECT c FROM ChatRoom c WHERE c.chatRoomUniqueId = :chatRoomUniqueID"),
    @NamedQuery(name = "ChatRoom.findByChatRoomimage", query = "SELECT c FROM ChatRoom c WHERE c.chatRoomimage = :chatRoomimage"),
    @NamedQuery(name = "ChatRoom.findByChatRoomcreatedDate", query = "SELECT c FROM ChatRoom c WHERE c.chatRoomcreatedDate = :chatRoomcreatedDate"),
    @NamedQuery(name = "ChatRoom.findByChatRoomlabel", query = "SELECT c FROM ChatRoom c WHERE c.chatRoomlabel = :chatRoomlabel")})
public class ChatRoom implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "ChatRoom_salt")
    private byte[] chatRoomsalt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ChatRoom_iterations")
    private int chatRoomiterations;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idChatRoom")
    private Integer idChatRoom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "ChatRoom_name")
    private String chatRoomname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "ChatRoom_image")
    private String chatRoomimage;
    @NotNull
    @Column(name = "ChatRoom_createdDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date chatRoomcreatedDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "ChatRoom_label")
    private String chatRoomlabel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "ChatRoom_uniqueId")
    private String chatRoomUniqueId;

    public ChatRoom() {
    }

    public ChatRoom(Integer idChatRoom) {
        this.idChatRoom = idChatRoom;
    }

    public ChatRoom(Integer idChatRoom, String chatRoomname, String chatRoomimage, String chatRoomlabel) {
        this.idChatRoom = idChatRoom;
        this.chatRoomname = chatRoomname;
        this.chatRoomimage = chatRoomimage;
        this.chatRoomlabel = chatRoomlabel;
    }

    public Integer getIdChatRoom() {
        return idChatRoom;
    }

    public void setIdChatRoom(Integer idChatRoom) {
        this.idChatRoom = idChatRoom;
    }

    public String getChatRoomname() {
        return chatRoomname;
    }

    public void setChatRoomname(String chatRoomname) {
        this.chatRoomname = chatRoomname;
    }

    public String getChatRoomimage() {
        return chatRoomimage;
    }

    public void setChatRoomimage(String chatRoomimage) {
        this.chatRoomimage = chatRoomimage;
    }

    public Date getChatRoomcreatedDate() {
        return chatRoomcreatedDate;
    }

    public void setChatRoomcreatedDate(Date chatRoomcreatedDate) {
        this.chatRoomcreatedDate = chatRoomcreatedDate;
    }

    public String getChatRoomlabel() {
        return chatRoomlabel;
    }

    public void setChatRoomlabel(String chatRoomlabel) {
        this.chatRoomlabel = chatRoomlabel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idChatRoom != null ? idChatRoom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChatRoom)) {
            return false;
        }
        ChatRoom other = (ChatRoom) object;
        if ((this.idChatRoom == null && other.idChatRoom != null) || (this.idChatRoom != null && !this.idChatRoom.equals(other.idChatRoom))) {
            return false;
        }
        return true;
    }


    public String getChatRoomUniqueId() {
        return chatRoomUniqueId;
    }

    public void setChatRoomUniqueId(String chatRoomUniqueId) {
        this.chatRoomUniqueId = chatRoomUniqueId;
    }

    @Override
    public String toString() {
        return "com.pz.playchat.chat.entity.ChatRoom[ idChatRoom=" + idChatRoom + " ]";
    }

    public byte[] getChatRoomsalt() {
        return chatRoomsalt;
    }

    public void setChatRoomsalt(byte[] chatRoomsalt) {
        this.chatRoomsalt = chatRoomsalt;
    }

   

    public void setChatRoomiterations(int chatRoomiterations) {
        this.chatRoomiterations = chatRoomiterations;
    }

}
