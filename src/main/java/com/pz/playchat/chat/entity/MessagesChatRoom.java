/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcelino
 */
@Entity
@Table(name = "Messages_ChatRoom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MessagesChatRoom.findAll", query = "SELECT m FROM MessagesChatRoom m"),
    @NamedQuery(name = "MessagesChatRoom.findByIdMessages", query = "SELECT m FROM MessagesChatRoom m WHERE m.idMessages = :idMessages"),
    @NamedQuery(name = "MessagesChatRoom.findMessagesByRoom", query = "SELECT m FROM MessagesChatRoom m WHERE m.chatRoomidChatRoom = :room"),
    @NamedQuery(name = "MessagesChatRoom.findByMessagescreatedDate", query = "SELECT m FROM MessagesChatRoom m WHERE m.messagescreatedDate = :messagescreatedDate")})
public class MessagesChatRoom implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMessages")
    private Integer idMessages;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "Messages_content")
    private byte[] messagescontent;
    @Column(name = "Messages_createdDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date messagescreatedDate;
        @JoinColumn(name = "ChatRoom_idChatRoom", referencedColumnName = "idChatRoom")
    @ManyToOne(optional = false)
    private ChatRoom chatRoomidChatRoom;
    @JoinColumn(name = "User_idUser", referencedColumnName = "idUser")
    @ManyToOne(optional = false)
    private User useruserId;

    public MessagesChatRoom() {
    }

    public MessagesChatRoom(Integer idMessages) {
        this.idMessages = idMessages;
    }

    public MessagesChatRoom(Integer idMessages, byte[] messagescontent) {
        this.idMessages = idMessages;
        this.messagescontent = messagescontent;
    }

    public Integer getIdMessages() {
        return idMessages;
    }

    public void setIdMessages(Integer idMessages) {
        this.idMessages = idMessages;
    }

    public byte[] getMessagescontent() {
        return messagescontent;
    }

    public void setMessagescontent(byte[] messagescontent) {
        this.messagescontent = messagescontent;
    }

    public Date getMessagescreatedDate() {
        return messagescreatedDate;
    }

    public void setMessagescreatedDate(Date messagescreatedDate) {
        this.messagescreatedDate = messagescreatedDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMessages != null ? idMessages.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessagesChatRoom)) {
            return false;
        }
        MessagesChatRoom other = (MessagesChatRoom) object;
        if ((this.idMessages == null && other.idMessages != null) || (this.idMessages != null && !this.idMessages.equals(other.idMessages))) {
            return false;
        }
        return true;
    }

    public ChatRoom getChatRoomidChatRoom() {
        return chatRoomidChatRoom;
    }

    public void setChatRoomidChatRoom(ChatRoom chatRoomidChatRoom) {
        this.chatRoomidChatRoom = chatRoomidChatRoom;
    }

    public User getUseruserId() {
        return useruserId;
    }

    public void setUseruserId(User useruserId) {
        this.useruserId = useruserId;
    }

    @Override
    public String toString() {
        return "com.pz.playchat.chat.entity.MessagesChatRoom[ idMessages=" + idMessages + " ]";
    }
    
}
