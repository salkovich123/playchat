/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marcelino
 */
@Entity
@Table(name = "RoomKeys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Keys.findAll", query = "SELECT k FROM Keys k"),
    @NamedQuery(name = "Keys.findByIdKeys", query = "SELECT k FROM Keys k WHERE k.idKeys = :idKeys"),
    @NamedQuery(name = "Keys.findByKeyscreatedDate", query = "SELECT k FROM Keys k WHERE k.keyscreatedDate = :keyscreatedDate"),
    @NamedQuery(name = "Keys.findKeyByDate", query = "SELECT k FROM Keys k WHERE k.chatRoomidChatRoom = :chatRoom AND k.keyscreatedDate < :date AND k.keysexpireDate > :date"),
    @NamedQuery(name = "Keys.findCurentKey", query = "SELECT k FROM Keys k WHERE k.chatRoomidChatRoom = :chatRoom AND k.keyscreatedDate < :date AND k.keysexpireDate IS NULL"),
    @NamedQuery(name = "Keys.findByChatRoom", query = "SELECT k FROM Keys k WHERE k.chatRoomidChatRoom = :chatRoom and k.keysexpireDate IS NULL"),
    @NamedQuery(name = "Keys.findByKeysexpireDate", query = "SELECT k FROM Keys k WHERE k.keysexpireDate IS NULL")})
public class Keys implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idKeys")
    private Integer idKeys;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "Keys_value")
    private byte[] keysvalue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Keys_createdDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyscreatedDate;
    @Column(name = "Keys_expireDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date keysexpireDate;
    @JoinColumn(name = "ChatRoom_idChatRoom", referencedColumnName = "idChatRoom")
    @ManyToOne(optional = false)
    private ChatRoom chatRoomidChatRoom;

    public Keys() {
    }

    public Keys(Integer idKeys) {
        this.idKeys = idKeys;
    }

    public Keys(Integer idKeys, byte[] keysvalue, Date keyscreatedDate) {
        this.idKeys = idKeys;
        this.keysvalue = keysvalue;
        this.keyscreatedDate = keyscreatedDate;
    }

    public Integer getIdKeys() {
        return idKeys;
    }

    public void setIdKeys(Integer idKeys) {
        this.idKeys = idKeys;
    }

    public byte[] getKeysvalue() {
        return keysvalue;
    }

    public void setKeysvalue(byte[] keysvalue) {
        this.keysvalue = keysvalue;
    }

    public Date getKeyscreatedDate() {
        return keyscreatedDate;
    }

    public void setKeyscreatedDate(Date keyscreatedDate) {
        this.keyscreatedDate = keyscreatedDate;
    }

    public Date getKeysexpireDate() {
        return keysexpireDate;
    }

    public void setKeysexpireDate(Date keysexpireDate) {
        this.keysexpireDate = keysexpireDate;
    }

    public ChatRoom getChatRoomidChatRoom() {
        return chatRoomidChatRoom;
    }

    public void setChatRoomidChatRoom(ChatRoom chatRoomidChatRoom) {
        this.chatRoomidChatRoom = chatRoomidChatRoom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKeys != null ? idKeys.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Keys)) {
            return false;
        }
        Keys other = (Keys) object;
        if ((this.idKeys == null && other.idKeys != null) || (this.idKeys != null && !this.idKeys.equals(other.idKeys))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pz.playchat.chat.entity.Keys[ idKeys=" + idKeys + " ]";
    }
    
}
