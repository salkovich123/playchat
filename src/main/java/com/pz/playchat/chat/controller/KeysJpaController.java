/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.Keys;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Marcelino
 */
@Stateless
public class KeysJpaController implements Serializable {

    @PersistenceUnit(name = "playChatPU")
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Keys keys) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.persist(keys);
            em.flush();
        }catch (javax.validation.ConstraintViolationException cve) {
            Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
            for (ConstraintViolation<?> cv : cvs) {
                System.out.println("------------------------------------------------");
                System.out.println("Violation: " + cv.getMessage());
                System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                // The violation occurred on a leaf bean (embeddable)
                if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                    System.out.println("Embeddable: "
                            + cv.getLeafBean().getClass().getSimpleName());
                }
                System.out.println("Attribute: " + cv.getPropertyPath());
                System.out.println("Invalid value: " + cv.getInvalidValue());
            }
        } finally{
            em.close();
        }
    }
    public Keys findKeysByRoom(ChatRoom room)throws ConstraintViolationException,TransactionRolledbackLocalException{
        EntityManager em = getEntityManager();
        Keys keys = new Keys();
        try{
            Query query = em.createNamedQuery("Keys.findByChatRoom",Keys.class);
            query.setParameter("chatRoom", room);
            keys =(Keys) query.getSingleResult();
        }catch(Exception e){
        return null;
        }finally{
            em.close();
        }
        return keys;
    }

    public void edit(Keys keys) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.merge(keys);
            em.flush();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
           em.remove(findKeys(id));
           em.flush();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
    }
   

    public List<Keys> findKeysEntities() {
        return findKeysEntities(true, -1, -1);
    }

    public List<Keys> findKeysEntities(int maxResults, int firstResult) {
        return findKeysEntities(false, maxResults, firstResult);
    }

    private List<Keys> findKeysEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Keys.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Keys findKeys(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Keys.class, id);
        } finally {
            em.close();
        }
    }

    public int getKeysCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Keys> rt = cq.from(Keys.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Keys findKeyForMessage(Date createdMessage, ChatRoom room){
        EntityManager em = getEntityManager();
        Keys key = new Keys();
        try{
            Query query = em.createNamedQuery("Keys.findKeyByDate", Keys.class);
            query.setParameter("chatRoom", room);
            query.setParameter("date", createdMessage);
            key =(Keys) query.getSingleResult();
        }catch(Exception e){
            System.out.println("Error in getting keys!!!  " + e.getMessage());
            Query query = em.createNamedQuery("Keys.findCurentKey",Keys.class);
            query.setParameter("chatRoom", room);
            query.setParameter("date", createdMessage);
            key = (Keys) query.getSingleResult();
            
        }finally{
            em.close();
        }
        
        System.out.println("Found key id: " + key.getIdKeys() + " createdDate: " + key.getKeyscreatedDate());
        return key;
    }
}
