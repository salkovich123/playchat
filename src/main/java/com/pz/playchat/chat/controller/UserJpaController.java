/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;

/**
 *
 * @author Marcelino
 */
@Stateless
public class UserJpaController implements Serializable {

   @PersistenceUnit(name = "playChatPU")
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(User user) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try{
            em.persist(user);
            em.flush();
        }catch (javax.validation.ConstraintViolationException cve)
          {
             Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
               for (ConstraintViolation<?> cv : cvs) {      
                  System.out.println("------------------------------------------------");
                  System.out.println("Violation: " + cv.getMessage());
                  System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                  // The violation occurred on a leaf bean (embeddable)
                    if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                       System.out.println("Embeddable: " +
                       cv.getLeafBean().getClass().getSimpleName());
                    }
                  System.out.println("Attribute: " + cv.getPropertyPath());
                  System.out.println("Invalid value: " + cv.getInvalidValue());
          }
          }finally{
            em.close();
        }
    }

    public void edit(User user) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em =getEntityManager();
        try{
            em.merge(user);
            em.flush();
        }catch(Exception e){
            System.out.println("Napaka!");
            
        }finally{
            em.close();
        }
       
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em =getEntityManager();
        try{
            em.remove(findUser(id));
            em.flush();
        }catch(Exception e){
            System.out.println("");
        }finally{
            em.close();
        }
       
    }
public User findByUserUniqueId(String uniqueId){
    EntityManager em = getEntityManager();
    try{
        Query query = em.createNamedQuery("User.findByUseruniqueId", User.class);
        query.setParameter("useruniqueId", uniqueId);
        System.out.println("User: "+ (User) query.getSingleResult());
        return (User) query.getSingleResult();
    }catch(Exception e){
        System.out.println("Napaka userUniqueId" + e.getMessage());
        return null;
    }
    finally{
        em.close();
    }
}

    public boolean findByUserEmail(String email){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("User.findByEmail", User.class);
            User user = (User) query.getSingleResult();
            if(user.getUseremail().length()>0){
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            return false;
        }
    }
    public List<User> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }
public List<User> findActiveUsers(){
    EntityManager em = getEntityManager();
    List<User> users =new ArrayList<User>();
    try{
        Query query = em.createNamedQuery("User.findByUserconfirmed",User.class);
        query.setParameter("userconfirmed", true);
       users =  (List<User>) query.getResultList();
    }catch(Exception e){
        System.out.println("Napaka");
    }finally{
        em.close();
    }
    return users;
}
    public List<User> findUserEntities(int maxResults, int firstResult) {
        return findUserEntities(false, maxResults, firstResult);
    }

    private List<User> findUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(User.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public User findUser(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(User.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<User> rt = cq.from(User.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public void testQuery(){
    
        EntityManager em = getEntityManager();
        try{
         Query query = em.createNamedQuery("User.findByloginQuery",User.class);
         query.setParameter("userusername", "salko");
         query.getSingleResult();
        }
         catch (javax.validation.ConstraintViolationException cve)
          {
             Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
               for (ConstraintViolation<?> cv : cvs) {      
                  System.out.println("------------------------------------------------");
                  System.out.println("Violation: " + cv.getMessage());
                  System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                  // The violation occurred on a leaf bean (embeddable)
                    if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                       System.out.println("Embeddable: " +
                       cv.getLeafBean().getClass().getSimpleName());
                    }
                  System.out.println("Attribute: " + cv.getPropertyPath());
                  System.out.println("Invalid value: " + cv.getInvalidValue());
          }
          }finally{
            em.close();
        }
      
    }
}
