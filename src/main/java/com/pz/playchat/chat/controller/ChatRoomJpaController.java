/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.IllegalOrphanException;
import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.PreexistingEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.ChatRoom;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.pz.playchat.chat.entity.Keys;
import java.util.ArrayList;
import java.util.List;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import com.pz.playchat.chat.entity.User;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;

/**
 *
 * @author Marcelino
 */
@Stateless
public class ChatRoomJpaController implements Serializable {

    @EJB
    ChatRoomUsersJpaController roomUsersJpa;
    
    @PersistenceUnit(name = "playChatPU")
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ChatRoom chatRoom) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.persist(chatRoom);
            em.flush();

        } catch (javax.validation.ConstraintViolationException cve) {
            Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
            for (ConstraintViolation<?> cv : cvs) {
                System.out.println("------------------------------------------------");
                System.out.println("Violation: " + cv.getMessage());
                System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                // The violation occurred on a leaf bean (embeddable)
                if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                    System.out.println("Embeddable: "
                            + cv.getLeafBean().getClass().getSimpleName());
                }
                System.out.println("Attribute: " + cv.getPropertyPath());
                System.out.println("Invalid value: " + cv.getInvalidValue());
            }
        } finally {
            em.close();
        }
    }

    public void edit(ChatRoom chatRoom) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.merge(chatRoom);
            em.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.remove(findChatRoom(id));
            em.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }
public ChatRoom findChatRoomByUniqueId(String id){
    EntityManager em = getEntityManager();
    ChatRoom room = new ChatRoom();
    try{
        
        Query query = em.createNamedQuery("ChatRoom.findByChatRoomUniqueId",ChatRoom.class);
        query.setParameter("chatRoomUniqueID", id);
        room = (ChatRoom)query.getSingleResult();
        
    }catch(Exception e){
        System.out.println("Error at finding chatRoom unique id!  Cause:" + e.getMessage());
    }finally{
        em.close();
    }
    return room;
}

    public List<ChatRoom> findChatRoomEntities() {
        return findChatRoomEntities(true, -1, -1);
    }

    public List<ChatRoom> findChatRoomEntities(int maxResults, int firstResult) {
        return findChatRoomEntities(false, maxResults, firstResult);
    }

    private List<ChatRoom> findChatRoomEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ChatRoom.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ChatRoom findChatRoom(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ChatRoom.class, id);
        } finally {
            em.close();
        }
    }

    public int getChatRoomCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ChatRoom> rt = cq.from(ChatRoom.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public User getChatRoomHolder(ChatRoom room) {
        EntityManager em = getEntityManager();
    
        int id = 0;
        try {
            Query query = em.createNativeQuery("SELECT ChatRoomUsers.idChatRoomUsers  FROM ChatRoomUsers WHERE ChatRoomUsers.ChatRoomUsers_joinedDate = (select MIN(ChatRoomUsers.ChatRoomUsers_joinedDate) from ChatRoomUsers where ChatRoomUsers.ChatRoom_idChatRoom = ?);");
            query.setParameter(1, room.getIdChatRoom());
            id = (int) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("ERROR" + e.getMessage());
        } finally {
            em.close();
        }
        return roomUsersJpa.findChatRoomUsers(id).getUseruserId();
    }

    public ChatRoom getChatRoomByLabel(String label) {
        EntityManager em = getEntityManager();
        ChatRoom room = new ChatRoom();
        try {
            Query query = em.createNamedQuery("ChatRoom.findByChatRoomlabel", ChatRoom.class);
            query.setParameter("chatRoomlabel", label);
            room = (ChatRoom) query.getSingleResult();
        } catch (Exception e) {

        } finally {
            em.close();
        }
        return room;
    }

    public ChatRoom findChatRoomByName(String roomName) {

        EntityManager em = getEntityManager();
        ChatRoom room = new ChatRoom();
        try {
            Query query = em.createNamedQuery("ChatRoom.findByChatRoomname", ChatRoom.class);
            query.setParameter("chatRoomname", roomName);
            room = (ChatRoom) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Napaka" + e.getMessage());
        } finally {
            em.close();
        }
        return room;
    }

}
