/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.PreexistingEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import com.pz.playchat.chat.entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;

/**
 *
 * @author Marcelino
 */
@Stateless
public class MessagesChatRoomJpaController implements Serializable {

    @PersistenceUnit(name = "playChatPU")
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MessagesChatRoom messagesChatRoom) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.persist(messagesChatRoom);
            em.flush();
        } catch (javax.validation.ConstraintViolationException cve) {
            Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
            for (ConstraintViolation<?> cv : cvs) {
                System.out.println("------------------------------------------------");
                System.out.println("Violation: " + cv.getMessage());
                System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                // The violation occurred on a leaf bean (embeddable)
                if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                    System.out.println("Embeddable: "
                            + cv.getLeafBean().getClass().getSimpleName());
                }
                System.out.println("Attribute: " + cv.getPropertyPath());
                System.out.println("Invalid value: " + cv.getInvalidValue());
            }
        } finally {
            em.close();
        }
    }

    public void edit(MessagesChatRoom messagesChatRoom) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.merge(messagesChatRoom);
            em.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.remove(findMessagesChatRoom(id));
            em.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public List<MessagesChatRoom> findMessagesByRoom(ChatRoom room) {
        EntityManager em = getEntityManager();
        List<MessagesChatRoom> messages = new ArrayList<MessagesChatRoom>();
        try {
            Query query = em.createNamedQuery("MessagesChatRoom.findMessagesByRoom", MessagesChatRoom.class);
            query.setParameter("room", room);
            messages = query.getResultList();
        } catch (Exception e) {
            System.out.println("Napaka findmessagesByRoom" + e.getMessage());
        } finally {
            em.close();
        }
        return messages;
    }

    public List<MessagesChatRoom> findMessagesChatRoomEntities() {
        return findMessagesChatRoomEntities(true, -1, -1);
    }

    public List<MessagesChatRoom> findMessagesChatRoomEntities(int maxResults, int firstResult) {
        return findMessagesChatRoomEntities(false, maxResults, firstResult);
    }

    private List<MessagesChatRoom> findMessagesChatRoomEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MessagesChatRoom.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MessagesChatRoom findMessagesChatRoom(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MessagesChatRoom.class, id);
        } finally {
            em.close();
        }
    }

    public int getMessagesChatRoomCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MessagesChatRoom> rt = cq.from(MessagesChatRoom.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public MessagesChatRoom findLastMessage(ChatRoom room) {
        EntityManager em = getEntityManager();
        List<MessagesChatRoom> mess = new ArrayList();
        try {
            Query q = em.createNamedQuery("MessagesChatRoom.findMessagesByRoom", MessagesChatRoom.class);
            q.setParameter("room", room);
            mess = q.getResultList();

        } catch (Exception e) {
            System.out.println("Error  at finding last message: " + e.getMessage());
        } finally {
            em.close();
        }
        return mess.get(mess.size() - 1);
    }

}
