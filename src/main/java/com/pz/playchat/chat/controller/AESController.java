/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.Keys;
import com.pz.playchat.chat.entity.MessagesChatRoom;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.crypto.JcaCipherService;
import org.apache.shiro.crypto.OperationMode;
import org.apache.shiro.crypto.PaddingScheme;
import org.apache.shiro.util.ByteSource;

/**
 *
 * @author Marcelino
 */
@Stateless
public class AESController implements Serializable {

    @EJB
    KeysJpaController keysJpa;

    AesCipherService cipherService;

    public AESController() {

        //init CipherService
        cipherService = new AesCipherService(); 
      
    }

    public void generateKey(ChatRoom room)  {

            //if key expiry date is null and key for room doesn't exists it will throw exception
            Keys previousKey = keysJpa.findKeysByRoom(room);
          
            System.out.println("Previous key:  " + previousKey);
                if(previousKey == null){
                    System.out.println("key ne obstaja generiram key");
                    Keys key1 = new Keys();
                     key1.setChatRoomidChatRoom(room);
                key1.setKeysvalue(cipherService.generateNewKey().getEncoded());
                key1.setKeyscreatedDate(new Date());
         try { 
             keysJpa.create(key1);
         } catch (Exception ex1) {
             Logger.getLogger(AESController.class.getName()).log(Level.SEVERE, null, ex1);
         }
                }else{
                try {
                    System.out.println("zaključujem key in naredim novega");
                    previousKey.setKeysexpireDate(new Date());
                    keysJpa.edit(previousKey);
                    //generate new key
                    Keys key = new Keys();
                    key.setChatRoomidChatRoom(room);
                    key.setKeysvalue(cipherService.generateNewKey().getEncoded());
                    key.setKeyscreatedDate(new Date());
                    keysJpa.create(key);
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(AESController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(AESController.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
                    
                /*    
               */
        
           
          
    
    }

    public byte[] encrypt(String message,ChatRoom room) {
     Keys keys = keysJpa.findKeysByRoom(room);
       System.out.println("\n \n \n Keys in encrypt method" +keys.getKeyscreatedDate() + " message "+ message);
    ByteSource source = cipherService.encrypt(message.getBytes(), keysJpa.findKeysByRoom(room).getKeysvalue());
    return source.getBytes();
    
    
    }
      public ByteSource encryptTest(String message,byte[] key) {
         
         
  ByteSource source = null;
        try {
            source = cipherService.encrypt(message.getBytes("UTF-8"),key);
            System.out.println("Encrypted :  " + source.toString());
                  System.out.println("Length of encrypted string in encryption  " + source.getBytes().length);
            return source;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AESController.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    return source;
            
    
    }

    public String decrypt(MessagesChatRoom mess,ChatRoom room) {
      Keys key =   keysJpa.findKeyForMessage(mess.getMessagescreatedDate(), room);
      System.out.println("i got a keyy!!!!   " + key.getIdKeys() + "DATE created: " + key.getKeyscreatedDate() + "key expire " +key.getKeysexpireDate());
       cipherService.setKeySize(256);
   cipherService.setMode(OperationMode.CBC);
    ByteSource source = cipherService.decrypt(mess.getMessagescontent(), key.getKeysvalue());
    System.out.println("source:  " + source.toHex());
    return new String(source.getBytes());
    }
    public String decryptTest(byte [] encrypted , byte[] key){
      
        System.out.println("length of encrypted string in decryption: " + encrypted.length + "\n encrypted: "+ encrypted.toString());
    ByteSource source = cipherService.decrypt(encrypted, key);
    System.out.println("Result of decryption:  base64  " + source.toBase64() + "Hex:  " + source.toHex());
    String s = new String(source.getBytes());
    System.out.println("Text string decrypted:  " +s);
   

    return source.toHex();
    }
}
     

