/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.chat.controller;

import com.pz.playchat.chat.controller.exceptions.NonexistentEntityException;
import com.pz.playchat.chat.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.pz.playchat.chat.entity.ChatRoom;
import com.pz.playchat.chat.entity.ChatRoomUsers;
import com.pz.playchat.chat.entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolation;

/**
 *
 * @author Marcelino
 */
@Stateless
public class ChatRoomUsersJpaController implements Serializable {

    @EJB
    ChatRoomJpaController chatRoom;
    @EJB
    UserJpaController userJpa;
    
   @PersistenceUnit(name = "playChatPU")
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ChatRoomUsers chatRoomUsers) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try{
            em.persist(chatRoomUsers);
            em.flush();
        }catch (javax.validation.ConstraintViolationException cve)
          {
             Set<ConstraintViolation<?>> cvs = cve.getConstraintViolations();
               for (ConstraintViolation<?> cv : cvs) {      
                  System.out.println("------------------------------------------------");
                  System.out.println("Violation: " + cv.getMessage());
                  System.out.println("Entity: " + cv.getRootBeanClass().getSimpleName());
                  // The violation occurred on a leaf bean (embeddable)
                    if (cv.getLeafBean() != null && cv.getRootBean() != cv.getLeafBean()) {
                       System.out.println("Embeddable: " +
                       cv.getLeafBean().getClass().getSimpleName());
                    }
                  System.out.println("Attribute: " + cv.getPropertyPath());
                  System.out.println("Invalid value: " + cv.getInvalidValue());
          }
          }finally{
            em.close();
        }
     
    }

    public void edit(ChatRoomUsers chatRoomUsers) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            em.merge(chatRoomUsers);
            em.flush();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        try {
            ChatRoomUsers usrs = em.getReference(ChatRoomUsers.class, id);
            em.remove(usrs);
    
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
    }
public ChatRoomUsers confirmInvitation(String userId, String roomId){
    EntityManager em = getEntityManager();
    ChatRoomUsers invitationInstance = new ChatRoomUsers();
    
    try{
        Query query = em.createNamedQuery("ChatRoomUsers.findByChatRoomUsersRoom", ChatRoomUsers.class);
        query.setParameter("chatRoom", chatRoom.findChatRoomByUniqueId(roomId));
        query.setParameter("user", userJpa.findByUserUniqueId(userId));
        invitationInstance = (ChatRoomUsers) query.getSingleResult();
        
    }catch(Exception e){
        System.out.println("Error at confirmation invite:   " + e.getMessage());
        invitationInstance = null;
    }finally{
        em.close();
    }
    return invitationInstance;
}
    public List<ChatRoomUsers> findChatRoomUsersEntities() {
        return findChatRoomUsersEntities(true, -1, -1);
    }

    public List<ChatRoomUsers> findChatRoomUsersEntities(int maxResults, int firstResult) {
        return findChatRoomUsersEntities(false, maxResults, firstResult);
    }

    private List<ChatRoomUsers> findChatRoomUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ChatRoomUsers.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
public ChatRoomUsers findUserInChatRoom(User user, ChatRoom room) throws Exception{
    EntityManager em = getEntityManager();
    ChatRoomUsers usrs = new ChatRoomUsers();
    try{
        Query query = em.createNamedQuery("ChatRoomUsers.findByChatRoomUsersRoom",ChatRoomUsers.class);
        query.setParameter("chatRoom", room);
        query.setParameter("user",user);
        usrs =(ChatRoomUsers) query.getSingleResult();
        
    }finally{
     em.close();
    }
    return usrs;
}
    public ChatRoomUsers findChatRoomUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ChatRoomUsers.class, id);
        } finally {
            em.close();
        }
    }

    public int getChatRoomUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ChatRoomUsers> rt = cq.from(ChatRoomUsers.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<ChatRoomUsers> findRoomsByUser(User i) {
       EntityManager em = getEntityManager();
       List<ChatRoomUsers> rooms = null;
       try{
          Query query = em.createNamedQuery("ChatRoomUsers.findByChatRoomUsersID", ChatRoomUsers.class);
          query.setParameter("userID", i);
          rooms = query.getResultList();
          System.out.println("Rooms size: " + rooms.size());
       }catch(Exception e){
         System.out.println("error!!!! findRoomsByUser "+ e.getMessage());
       }finally{
           em.close();
       }
            return rooms;
    }
    public List<User> ChatRoomUsers(ChatRoom room){
        EntityManager em = getEntityManager();
        List<User> users = new ArrayList<User>();
        List<ChatRoomUsers> usersInRoom = null;
          
        try{
            Query query =  em.createNamedQuery("ChatRoomUsers.findByChatRoom");
            query.setParameter("chatRoom", room);
            usersInRoom = query.getResultList();
            System.out.println(" users in room " + usersInRoom.size());
            for(int i = 0;i< usersInRoom.size();i++){
                users.add(usersInRoom.get(i).getUseruserId());
            }
        }catch(Exception e){
            System.out.println("Napaka" + e.getMessage());
        }finally{
            em.close();
        }
        return users;
    
    }
    
    
    public List<ChatRoomUsers> getChatRoomUsersByRoom(ChatRoom room){
        EntityManager em = getEntityManager();
        List<ChatRoomUsers> roomUsers = null;
        try{
            Query query = em.createNamedQuery("ChatRoomUsers.findByChatRoom",ChatRoomUsers.class);
            query.setParameter("chatRoom", room);
            roomUsers =(List<ChatRoomUsers>) query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
        return roomUsers;
    }
    public int countUsersInRoom(ChatRoom room) {
        EntityManager em = getEntityManager();
        
        try{
            Query query = em.createNamedQuery("ChatRoomUsers.findByChatRoom", ChatRoomUsers.class);
            query.setParameter("chatRoom", room);
            return query.getResultList().size();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            em.close();
        }
return 0;
    }
    
}
