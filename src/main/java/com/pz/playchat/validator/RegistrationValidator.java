/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.validator;

import com.pz.playchat.chat.controller.UserJpaController;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Marcelino
 */
@Named(value = "registrationValidator")
@ViewScoped
public class RegistrationValidator implements Serializable {

    @EJB
    UserJpaController user;

    public void validateEmail(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String validatedValue = (String) value;
  
        try {
            InternetAddress emailAddr = new InternetAddress(validatedValue);
            emailAddr.validate();

            if (user.findByUserEmail(validatedValue)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error. E-mail exists!!", null));

            }
        } catch (AddressException ex) {
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error. E-mail is invalid please insert proper email (e.g. example@eample.org)", null));

        }

    }

   

}
