/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.playchat.validator;

import com.pz.playchat.chat.controller.ChatRoomJpaController;
import com.pz.playchat.chat.controller.UserJpaController;
import com.pz.playchat.chat.entity.ChatRoom;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Marcelino
 */
@Named(value="roomLabelValidator")
@ViewScoped
public class RoomLabelValidator implements Serializable  {

  @EJB
  ChatRoomJpaController roomController;
  
public void validateRoomLabel(FacesContext context, UIComponent component, Object value) throws ValidatorException{
   ChatRoom croom = null;
   try{
        croom =  roomController.getChatRoomByLabel((String)value);
   }catch(Exception e){
       
   }
   System.out.println("Label in validator" + croom.getChatRoomlabel());
   if(croom.getChatRoomlabel()==null){
       
   }else{
       throw new ValidatorException(new FacesMessage("Room already exists! please select another name!"));
   }
   
    
}
    
}
